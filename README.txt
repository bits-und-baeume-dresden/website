# General information

This Repository contains the content and the metadata (templates, css) of the Website.
The build command is `bundle exec jekyll build --destination /var/www/virtual/bubdd/dresden.bits-und-baeume.org`.
It is automatically triggered from the post-recieve-hook in `~/git/dresden.bits-und-baeume.org`.
If you have any questions ask mcnesium or cark (see commit history for email addresses).

**Important: documentation on how to build the website is now in a separate repo:**
`ssh://bubdd@kojima.uberspace.de/home/bubdd/git/orga.git`


# Todo

- convert kih page from html into markdown
    - current situation: due to parallel work it exists twice inside the repo:
        - top level (kih.hml, kih_styles.css) with redirect in .htaccess
        - and inside kih2/ directory

- establish staging system. Ideas (inspired from https://staging.fsfw-dresden.de/)
    - determine the branch name which was pushed to
    - master branch is served at dresden.bits-und-baeume.org/
    - every other branch is served at bubdd.uber.space/staging/<branchname>/
    - take care of fancy branch names (slashes are probably OK -> create subdirectories)
    - temporary workarround: manually trigger build on ubserpace for branches different than master

- Try to deploy gitea at uberspace to enable users without ssh-access to write blog posts
- Polish the system such that it can be used by other BuB groups without much effort.
