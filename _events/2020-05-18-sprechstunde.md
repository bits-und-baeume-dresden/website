---
layout: event
title: Bits&Bäume-Sprechstunde
date: 2020-05-18 18:30
location: BigBlueButton Online-Konferenz
---

Mit welchen Tricks wird ein digitales Plenum wirklich produktiv? Welche Tools sind für welchen Zweck empfehlenswert und welche nicht? Wie können wir unseren Datenaustausch vor Überwachung schützen?

Die Bits&Bäume-Gruppe Dresden bietet eine regelmäßige Sprechstunde rund um IT-Fragen an, die sich speziell an Menschen und Gruppen der Dresdner Zivilgesellschaft richtet, und sich für die Aspekte Nachhaltigkeit und Datenschutz interessieren. Wir können bestimmt nicht jede Frage beantworten, aber wir bemühen uns. :) Immer Montags, 18:30 bis 19:30 Uhr.

Treffpunkt: BigBlueButton Online-Konferenzraum <https://dresden.bits-und-baeume.org/bbb>.

Kontakt: dresden{ät}bits-und-baeume.org
