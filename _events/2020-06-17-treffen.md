---
layout: event
title: Bits&Bäume-Treffen
date: 2020-06-17 19:00
location: BigBlueButton
---

Wir führen unsere Treffen aktuell mit dem freien Konferenztool BigBlueButton durch und nutzen je nach Verfügbarkeit verschiedene Instanzen. Link zur Instanz: <https://dresden.bits-und-baeume.org/bbb>.



