---
layout: event
title: Überregionales Bits&Bäume-Community-Treffen
date: 2021-05-06 19:31
location: Online (Senfcall).
---

Wir treffen uns virtuell: <https://lecture.senfcall.de/bit-f17-e0c-kny>

**Achtung:** Am gleichen Tag findet vorher ein internationales Online-Symposium mit Bits&Bäume-Bezug statt (Mehr Infos: [nachhaltige-digitalisierung.de/...](https://www.nachhaltige-digitalisierung.de/bits-baeume/forum-bits-baeume.html)). Deswegen wird es dieses Community-Treffen, nicht wie urspünglich geplant ein "großes" Treffen, mit mehren inhaltlichen Beiträgen, sondern ein normales kleineres mit Treffen für lockeren Austausch zwischen den regionalen Zweigen und interessierten Menschen.

