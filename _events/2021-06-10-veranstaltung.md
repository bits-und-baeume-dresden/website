---
layout: event
title: "Veranstaltung: „Wie leicht ist die Cloud?“"
date: 2021-06-10 19:00
location: Online
---

Gesprächsabend zum Rohstoffabbau und verantwortungsvollem Konsum von Technik.

Veranstaltung des ÖIZ Dresden mit Unterstützung von Bits&Bäume-Dresden.

Weitere Infos: <https://www.infozentrum-dresden.de/digital-und-nachhaltig/>

