---
layout: event
title: "Veranstaltung: „Zoom out!“"
date: 2021-06-17 19:00
location: Online
---

Gesprächsabend zur Macht von Internetkonzernen und dem eigenen Umgang mit Daten.


Weitere Infos: <https://www.infozentrum-dresden.de/digital-und-nachhaltig/>

