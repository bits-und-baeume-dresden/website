---
layout: event
title: "Dokumentarfilm: „Welcome to Sodom – Dein Smartphone ist schon hier“"
date: 2021-06-20 19:00
location: Online
---


„Welcome to Sodom – Dein Smartphone ist schon hier“ ist ein Dokumentarfilm zu Europas Elektroschrottmüllhalde in Ghana.

Er wird mit Untertiteln und deutschem Voiceover gezeigt. Dann anschließend Filmgespräch mit Alhassan Muniru, Projekt „Recycle Up Ghana“, der aus Ghana zugeschaltet ist


Weitere Infos: <https://www.infozentrum-dresden.de/digital-und-nachhaltig/>

