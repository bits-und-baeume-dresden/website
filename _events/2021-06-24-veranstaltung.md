---
layout: event
title: Digitale Fragmente einer Utopie
date: 2021-06-24 19:00
location: Online
---

"Digitale Fragmente einer Utopie" -- Diskussionsveranstaltung mit Impulsvortrag im Rahmen des [Themenfestivals "Digital ... und nachhaltig?"](https://www.infozentrum-dresden.de/digital-und-nachhaltig/)

Teilnahmelink: <https://indigo.collocall.de/oku-gp7-4d8-giw>


