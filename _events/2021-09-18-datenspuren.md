---
layout: event
title: Bits&Bäume bei den Datenspuren
date: 2021-09-18 16:00
location: Zentralwerk, Dresden Pieschen
---


Workshop: [Digitalisierung debuggen – Ein (toolbasierter) Low-Level-Ansatz aus der Bits&Bäume-Bewegung](https://talks.datenspuren.de/ds21/talk/PQSDPT/)

Weitere Infos:

- <https://datenspuren.de> (Startseite)
- [gesamtes Programm](https://talks.datenspuren.de/ds21/schedule/)
- [Anreise Zentralwerk](https://osm.org/go/0MLkHgy28-?m=)


