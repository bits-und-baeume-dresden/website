---
layout: event
title: überregionales Bits&Bäume-Community-Treffen
date: 2021-10-07 19:31
location: Online.
---

Wir treffen uns online. Nähere Infos: <https://wiki.bits-und-baeume.org/community-treffen/>.

