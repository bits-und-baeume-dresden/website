---
layout: event
title: überregionales Bits&Bäume-Vernetzungs-Treffen
date: 2021-11-04 19:31
location: Online.
---

Wir treffen uns online. Nähere Infos: <https://wiki.bits-und-baeume.org/community-treffen/>.

