---
layout: event
title: überregionales Bits&Bäume-Community-Treffen
date: 2021-12-02 18:00
location: Online.
---

Wir treffen uns online. Nähere Infos: <https://wiki.bits-und-baeume.org/community-treffen/>.

