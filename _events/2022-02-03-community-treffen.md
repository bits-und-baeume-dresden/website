---
layout: event
title: überregionales Bits&Bäume-Vernetzungs-Treffen
date: 2022-02-03 19:31
location: Online.
---

Wir treffen uns online. Nähere Infos: <https://wiki.bits-und-baeume.org/community-treffen/>.

