---
layout: event
title: überregionales Bits&Bäume-Community-Treffen
date: 2022-03-03 18:00
location: Online.
---

Wir treffen uns online. Nähere Infos: <https://wiki.bits-und-baeume.org/community-treffen/>.

