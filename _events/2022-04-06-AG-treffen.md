---
layout: event
title: Auftakttreffen für alle AGs zum Mitmachen bei der Konferenzorganisation
date: 2022-04-07 19:00
location: Online.
---


BBB-Raum: <https://lecture.senfcall.de/rai-qmi-0mh-ziw>


Nähere Infos: <https://bits-und-baeume.org/konferenzankuendigung/de>.
