---
layout: event
title: Bits&Bäume-Treffen
date: 2022-04-25 19:00
location: Online
---

Wir treffen uns virtuell: <https://dresden.bits-und-baeume.org/bbb.html>.

