---
layout: event
title: überregionales Bits&Bäume-Community-Treffen
date: 2022-12-01 18:00
location: Online.
---

Wir treffen uns online. Nähere Infos: <https://wiki.bits-und-baeume.org/community-treffen/>.

