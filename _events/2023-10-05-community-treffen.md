---
layout: event
title: überregionales Bits&Bäume-Community-Treffen
date: 2023-10-05 19:31
location: Online.
---

Wir treffen uns online. Nähere Infos: <https://discourse.bits-und-baeume.org/t/2023-10-05-online-community-treffen-19-31-uhr/1494>.

