import os
import datetime as d
from datetime import datetime as dt

try:
    # only needed for development and debugging
    # can be installed via pip install ipydex
    from ipydex import IPS
except ImportError:
    pass


import argparse


parser = argparse.ArgumentParser(epilog="typical argument: 14")
parser.add_argument(
    "future_days",
    help="number of days (from today) for which the events should be generated",
    type=int,
)


args = parser.parse_args()

def main(args):

    today = dt.today()
    for n in range(args.future_days):
        day =  today + d.timedelta(n)

        year, week, week_day = day.isocalendar()

        text = f"{day}, {week}, {week_day}: "

        if week_day == 1:
            # Monday
            # res = create_sprechstunde(day)  # old
            if week % 4 == 1:
                res = create_plenum(day)
        elif week_day == 1 and week % 2 == 1 :
            # Wednesday odd weeks
            pass
            # res = create_plenum(day)
        elif week_day == 4 and weekday_in_month(day) == 1 :
            # first Thursday in month
            res = create_community_meeting(day)
        else:
            res = "–"

        print(text, res)

def write_file(fname, txt):

    if os.path.isfile(fname):
        print("Omit existing file: ", fname)
        return


    with open(fname, "w", encoding="utf8") as txtfile:
        txtfile.write(txt)


def weekday_in_month(day: dt):
    """
    Return the "index" (beginning with 1) of the weekday of `day` in the corresponding month.

    This is used to dertermine e.g. if a day is the first (i.e. k=1) Thursday in a month
    """

    for k in range(1, 7):
        test_day = day - d.timedelta(days=7*k)
        if test_day.month != day.month:
            return k
    else:
        # the loop has not been left. This is unexpected
        raise ValueError("A specific weekday can occure 5 times at most in one month.")


def create_sprechstunde(day):
    txt = \
f"""---
layout: event
title: Bits&Bäume-Sprechstunde
date: {day.strftime("%Y-%m-%d")} 18:30
location: BigBlueButton Online-Konferenz
---

Mit welchen Tricks wird ein digitales Plenum wirklich produktiv? Welche Tools sind für welchen Zweck empfehlenswert und welche nicht? Wie können wir unseren Datenaustausch vor Überwachung schützen?

Die Bits&Bäume-Gruppe Dresden bietet eine regelmäßige Sprechstunde rund um IT-Fragen an, die sich speziell an Menschen und Gruppen der Dresdner Zivilgesellschaft richtet, und sich für die Aspekte Nachhaltigkeit und Datenschutz interessieren. Wir können bestimmt nicht jede Frage beantworten, aber wir bemühen uns. :)


<br>
## &nbsp;&nbsp;&nbsp; → Immer Montags, 18:30 bis 19:30 Uhr.
<br>



Treffpunkt: BigBlueButton Online-Konferenzraum <https://dresden.bits-und-baeume.org/bbb>.

Kurze Voranmeldung Empfehlenswert. Kontakt: dresden(ät)bits-und-baeume.org


"""
    fname = f"{day.strftime('%Y-%m-%d')}-sprechstunde.md"
    write_file(fname, txt)
    text = f"sprechstunde: {fname}"
    return text


def create_plenum(day):
    txt = \
f"""---
layout: event
title: Treffen Bits&Bäume-Dresden
date: "{day.strftime('%Y-%m-%d')} 20:15"
location: Kellerklub GAG 18 (Fritz-Löffler-Straße 16)
---

Wir treffen uns alle vier Wochen 20:15 Uhr im: [Kellerklub GAG 18](https://www.gag-18.com/anfahrt/) (Fritz-Löffler-Straße 16). Gäste und Interessierte Menschen sind herzlich willkommen. Uns hilft es bei der Planung der Treffen, wenn Ihr Euch vorher kurz meldet, per Mail an dresden{{ät}}bits-und-baeume.org oder im [Matrix Chat](https://matrix.to/#/#bubdd:matrix.org).

"""


    fname = f"{day.strftime('%Y-%m-%d')}-treffen.md"
    write_file(fname, txt)
    text = f"treffen: {fname}"
    return text

def create_community_meeting(day):
    start_time = "19:31"
    title = "überregionales Bits&Bäume-Vernetzungs-Treffen"
    if day.month % 3 == 0:
        start_time = "18:00"
        title = "überregionales Bits&Bäume-Community-Treffen"

    txt = \
f"""---
layout: event
title: {title}
date: {day.strftime("%Y-%m-%d")} {start_time}
location: Online.
---

Wir treffen uns online. Nähere Infos: <https://wiki.bits-und-baeume.org/community-treffen/>.

"""

    fname = f"{day.strftime('%Y-%m-%d')}-community-treffen.md"
    write_file(fname, txt)
    text = f"community: {fname}"
    return text




final_msg = \
"""
Jetzt:
git add .
git commit -m "neue Termine"
git push

"""

if __name__ == "__main__":
    main(args)

    print(final_msg)
