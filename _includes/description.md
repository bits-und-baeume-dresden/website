Digitalisierung und Nachhaltigkeit sind die entschei­denden, existenziellen Herausforderungen des 21. Jahrhunderts, werden bislang aber kaum gemeinsam gedacht oder diskutiert. Dabei liegen die Fragen auf der Hand: Wie kann die digitale Gesellschaft demokratisch und gerecht gestaltet werden? Wie kann sie darauf ausgerichtet werden, unsere Lebensgrundlagen auf diesem Planeten zu bewahren? Welche Rolle spielt Nachhaltigkeit für stabile Tech-Communities? Welche ökologischen Chancen stecken in digitalen Anwendungen z.B. für Klima- und Ressourcenschutz? Welche Art von Digitalisierung steht diesen Zielen entgegen?

Motiviert und inspiriert durch die Konferenz [Bits & Bäume 2018] haben sich ein paar Teilnehmer:innen daran gemacht, die Idee auf die lokale Ebene zu übertragen.



[Bits & Bäume 2018]: https://bits-und-baeume.org
