
<a href="https://lists.posteo.de/listinfo/bubdd">Info-Mailingliste</a>
<a href="https://tuuwi.de/wer-wir-sind/impressum/">Impressum</a>

<strong>Kontakt:</strong> <span class="mail_replace_info"><a href="mailto:dresden{ät}bits-und-baeume.org">dresden{ät}bits-und-baeume.org</a></span>

Datenschutzerklärung: die Logs des uber.space-Accounts sind deaktiviert, somit keine Datenerfassung. Dessen ungeachtet gilt die <a href="https://tuuwi.de/impressum/" target="_blank">Datenschutzerklärung</a> der TUUWI.


Diese Webseite ist eine modifizierte Version der Startseite der
<a href="https://bits-und-baeume.org">Bits&amp;Bäume-Konferenz 2018</a>.
Diese wurde vom <a href="https://kollektiv.afeefa.de/" target="_blank">Freien Kollektiv für Webentwicklung, IT-Beratung und Kommunikation</a>
konzipiert und umgesetzt.  <a href="https://github.com/afeefade/bitbaum" target="_blank">Quellcode auf GitHub.com</a>
