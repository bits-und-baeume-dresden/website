---
layout: post
title: Rückblick B&B 2018
date: 2019-04-02 11:40
author: cark
summary:
    Im November 2018 fand in Berlin mit fast 2.000 Teilnehmer:innen die bislang größte Konferenz zu Digitalisierung und Nachhaltigkeit statt - die Bits und Bäume.
---

Im November 2018 fand in Berlin mit fast 2.000 Teilnehmer:innen die bislang größte Konferenz zu Digitalisierung und Nachhaltigkeit statt - die [Bits & Bäume][1].

Neben jeder Menge Vernetzungsleistung, vielen [hochinteressanten Audio- und Videoaufzeichnungen][2] von Vorträgen und Diskussionen sind die [11 Forderungen der Bits & Bäume Konferenz 2018][3] das wesentliche Ergebnis dieser Konferenz.

[Fotogalerie] [Online-Programm] [PDF-Programm]



[1]: https://bits-und-baeume.org
[2]: https://media.ccc.de/c/bub2018
[3]: https://bits-und-baeume.org/forderungen/de

[Fotogalerie]: https://bits-und-baeume.org/fotogalerie
[Online-Programm]: https://fahrplan.bits-und-baeume.org/
[PDF-Programm]: https://bits-und-baeume.org/downloads/bits-und-baeume-2018-programmheft.pdf
