---
layout: post
title: Bits&Bäume wächst zur Bewegung!
date: 2019-05-01 12:34
author: cark
summary:
    Im Mai 2019 hat das Orga-Team der Berliner Konferenz der Bits & Bäume-Idee in die Freiheit entlassen. Es wird dazu aufgerufen lokal eigenen Treffen und Veranstaltungen zu Digitalisierung und Nachhaltigkeit realisieren. 
---

Im Mai 2019 hat das Orga-Team der Berliner Konferenz der Bits & Bäume-Idee in die Freiheit entlassen. [Es wird dazu aufgerufen][1], lokal eigenen Treffen und Veranstaltungen zu Digitalisierung und Nachhaltigkeit realisieren. 

Bits & Bäume Dresden war damit der erste Seitentrieb. Mögen viele weitere folgen. 

[1]: https://bits-und-baeume.org/waechst
