---
layout: post
title: Zu Gast beim Multicast
date: 2019-05-10 23:12
author: cark
---

Was für's Ohr: Wir waren zu Gast in der [elften Folge "Papier?!" des Multicast-Podcast][1] der AG DSN!

[1]: https://podcast.agdsn.de/2019/05/10/mc011-papier/
