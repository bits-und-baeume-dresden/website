---
layout: post
title: Ergebnisse der Bits&Bäume Dresden 2019
date: 2019-05-30 13:37
author: cark
summary:
    Der erste lokale Zweig der Bits & Bäume war ein voller Erfolg. Mit ca. 120 Besucher:innen wurden unsere Erwartungen weit übertroffen, was eindrucksvoll die Aktualität und das Interesse an der Thematik zeigt.
---

Der erste lokale Zweig der Bits & Bäume war ein voller Erfolg. Mit ca. 120 Besucher:innen wurden unsere Erwartungen weit übertroffen, was eindrucksvoll die Aktualität und das Interesse an der Thematik zeigt.

![](/assets/images/2019-05-23/bubdd-dom-01_1000.jpg)


Das Feedback der Teilnehmer:innen passt zu der Annahme: Digitalisierung und Nachhaltigkeit können nicht mehr weiter getrennt gedacht werden. Das haben auch die lebhaften Diskussionen gezeigt - für die die angeschlagenen 25 Minuten nicht einmal annähernd reichten. An den sieben gleichzeitig stattfindenen Thementischen wurden über vielfältige Aspekte diskutiert. Inhaltlich war einiges dabei: von den Arbeitsbedingungen bei dem Abbau von seltenen Erden oder dem Spannungsfeld zwischen der Langlebigkeit von Hardware und Innovationen bis zu Alltäglichem, wie den Zwang, einen bestimmten Messenger zu verwenden, um nicht sozial ausgeschlossen zu werden. Durch die überraschend hohe Teilnehmerzahl kam der Raum für sieben gleichzeitige Gruppendiskussionen an seine akustische Grenze - wir müssen zugeben, dass wir schlicht nicht mit so einem großen Andrang gerechnet haben. Die Teilnehmenden haben sich jedoch davon nicht abschrecken lassen und in der zweiten Diskussionsrunde die spontane Verlängerung genutzt, um sich bis nach 21 Uhr noch weiter auszutauschen.

![](/assets/images/2019-05-23/DSC07200_blur_1000.jpg)


Das Thema trifft offenbar einen Nerv und es besteht weiterer Diskussions- und Aktionsbedarf. Das war auch aus vielen Kommentaren zu entnehmen: "Wir wollen mehr!" / "Wunsch: Ganzer Tag (Samstag/Sonntag)" / "Bitte wiederholen!" etc. Das können wir als Orgateam uns auch ganz gut vorstellen, vielleicht dann mit etwas konkreteren Diskussionsfragen, die sich näher an "lokalen" "alltäglichen" Problemen orientieren - so wie es sich einige Teilnehmer:innen gewünscht haben. Ideen, was diese konkreteren Diskussionsthemen sein könnten, nehmen wir gerne entgegegen. Aber zuerst einmal wird es am 20.06 weitergehen, auf der [OUTPUT.DD] (Projektschau der Fakultät Informatik). Unter dem Titel "Nachaltiger Output statt gehyptem Nonsens" wird Juliane Krüger von der [Open Knowledge Foundation] ab 14:00 Uhr im [APB E023] den Hauptvortrag der Veranstaltung halten. Wir nutzen die Gelegenheit um die Ergebnisse der Diskussionsrunden (siehe Fotos) noch einmal aufzugreifen.

[OUTPUT.DD]: https://output-dd.de/programm/
[Open Knowledge Foundation]: https://okfn.de/
[APB E023]: https://navigator.tu-dresden.de/etplan/apb/00/raum/542100.2310


## Ergebnisse der Thementische


[![](/assets/images/2019-05-23/TT_Bildung_und_Demokratie_500.jpg)](/assets/images/2019-05-23/TT_Bildung_und_Demokratie_1000.jpg)

[![](/assets/images/2019-05-23/TT_Datenschutz_Kontrolle_von_Monopolen_500.jpg)](/assets/images/2019-05-23/TT_Datenschutz_Kontrolle_von_Monopolen_1000.jpg)

[![](/assets/images/2019-05-23/TT_Entwicklungs_Handelspolitische_Aspekte_500.jpg)](/assets/images/2019-05-23/TT_Entwicklungs_Handelspolitische_Aspekte_1000.jpg)

[![](/assets/images/2019-05-23/TT_Langlebigkeit_Software_und_Hardware_500.jpg)](/assets/images/2019-05-23/TT_Langlebigkeit_Software_und_Hardware_1000.jpg)

[![](/assets/images/2019-05-23/TT_sozial_oekologische_Gestaltung_Digi_500.jpg)](/assets/images/2019-05-23/TT_sozial_oekologische_Gestaltung_Digi_1000.jpg)

[![](/assets/images/2019-05-23/TT_IT-Sicherheit_500.png)](/assets/images/2019-05-23/TT_IT-Sicherheit_1000.png)

[![](/assets/images/2019-05-23/TT_Vernetzungstisch_500.jpg)](/assets/images/2019-05-23/TT_Vernetzungstisch1.jpg)





Wir wollen auf jeden Fall zusammen mit Euch an dem Thema dranbleiben. Eine Möglichkeit dazu ist der 1. Bits&Bäume Stammtisch. (20.06. 16:30, Alte Mensa (Ostseite)) Kommt gerne spontan vorbei. Wenn Ihr Euch einbringen wollt, könnt Ihr uns direkt ansprechen oder über dresden{ät}bits-und-baeume.org erreichen. Infos über zukünfitge Projekte oder Veranstaltungen versenden wir über die Info-Mailingliste.


Die Veranstaltung Bits&Bäume DD 2019.05 wurde unterstützt durch die TU Umweltinitiative im Rahmen der Festwoche "30 Jahre TUUWI".
