---
layout: post
title: Eine kurze und subjektive Historie der Bits&Bäume-Bewegung
date: 2020-03-14 13:00
author: cark
summary: Im November 2018 fand die 1. Bits&Bäume-Konferenz statt. Um was es dabei ging und wie daraus eine Bewegung wurde.
---



### November 2018: Konferenz "Bits & Bäume" in Berlin

- Ursprungs-Team: Ökologisch interessierter Dunstkreis des [CCC](https://www.ccc.de/)
	- → Trägerkreis der Konferenz wird gebildet
    - CCC, BUND, OpenKnowledgeFoundation, Brot für die Welt, [FIfF](https://www.fiff.de/), ...
- Leitidee: Digitalisierung und Nachhaltigkeit interagieren sehr stark. Bisher agieren NGOs hauptsächlich in ihrer Schublade und sind damit ggf. auch Teil des "anderen" Problems (unreflektierte SocialMedia-Nutzung durch Öko-NGOs, Energie- und Ressourcenverbrauch durch Techi-Veranstaltungen, ...).
- Grundsätzlich ähnliche **Herausforderungen** in beiden Bereichen:
    - Sich abzeichnende massive Probleme
    - weitgehendes Desinteresse des gesellschafltichen Mainstreams
    - problematischer Einfluss etablierter Partikularinteressen (z.B. Auto- bzw. Werbe-Industrie)
    - die Dominanz von an Bequemlichkeit orientierten privater Konsum- und Verhaltenssnormen
- Ziele der Konferenz:
    - Erfahrungs- und Wissensaustausch ✓
    - Vernetzung ✓
    - "Wir sind nicht alleine"-Gefühl ✓
- Konkrete Ergebnisse:
    - Veröffentlichung von [11 konkreten Forderungen](https://bits-und-baeume.org/forderungen/info/de)
    - Viele spannende [aufgezeichnete Vorträge](https://media.ccc.de/c/bub2018)
    - Buch: [Was Bits&Bäume verbindet](https://www.oekom.de/nc/buecher/gesamtprogramm/buch/was-bits-baeume-verbindet.html)


### Nach der Konferenz

- Orga-Team mit wird durch Alltagsaufgaben und andere Projekte okkupiert. Erstmal ziemliche Funksstille.
- Einzelne Konferenz-TN nehmen den Gedanken auf und organisieren ähnliche Zusammenkünfte
    - 35C3: [Hackers against Climate Crisis](https://hacc.uber.space/Main_Page), C3-Sustainability-Team
    - Mai/Juni 2019: 2 Veranstaltungen "Bits- und Bäume Dresden" (Vorträge + Diskussionen)
    - Offizielle Freigabe der Überschrift "Bits und Bäume" (und Domain) durch Konferenz-Orga und Einladung eine [**Bewegung**](bits-und-baeume.org/waechst/) daraus zu machen
    - Seit Sommer 2019 regelmäßige "Bits und Bäume"-Treffen in Berlin, [Dresden](https://dresden.bits-und-baeume.org/), später auch: Hannover
- Ende 2019: Es etablieren sich überregionale Kommunikationskanäle
    - Forum: <https://discourse.bits-und-baeume.org/>
        - Zweck: inhaltlicher Austausch
    - Mailingliste: <https://lists.posteo.de/listinfo/bitsundbaeume>
        - Zweck: informiert bleiben über das Thema

### Pläne für 2020:

- Vernetzung auf lokaler Ebene, inhaltliche Bildungsarbeit
- Unterstützung der Zivilgesellschaft bei der digitalen (Corona-)Resilienz (Telekonferenzen, ...)
- Teilnahme an kleineren und größeren Veranstaltungen (Workshops, Vorträge)
- Kooperation mit Politik zur Übersetzung einzelner Forderungen in Anträge für Gremien
- Erarbeitung von konkreten Handlungsempfehlungen (Nextcloud statt Dropbox, Geräte-Nutzungsdauer verlängern mit Freier Software)

### Fazit

- Herausforderung: "Echte Vernetzung" zwischen Bit- und Baum-affinen Akteur:innen
    - Aktuell dominieren "Bits"
- Angestrebt: Mehr regionale Gruppen und ggf. überregionale Gruppen mit speziellen Interessen
- Empfehlung für Interessierte: Anmelden bei [Forum](https://discourse.bits-und-baeume.org/)/[Mailingliste](https://lists.posteo.de/listinfo/bitsundbaeume)
