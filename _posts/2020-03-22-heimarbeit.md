---
layout: post
title: Infos zu kollaborativen Videocall- und Schreib-Tools
date: 2020-03-22 18:00
author: b&b berlin
summary: Die Berliner B&B-Gruppe hat in einem Vortrag die wichtigsten Tools und Tipps fürs Arbeiten und Engagieren in Zeiten von Kontaktbeschränkungen zusammengefasst.
---

(Mit freundlicher Erlaubnis der Urheber spiegeln wir hier die Beschreibung und Links eines Projekts der [Bits&Bäume-Gruppe Berlin](https://berlin.bits-und-baeume.org/))

---

Du machst Homeoffice und möchtest mit Kollegen kollaborativ Notizen aufschreiben? Du kannst deine Oma nicht besuchen, würdest sie aber gern sehen?

Dafür suchst du ein gutes Open-Source-Videotool, kennst aber nur Skype, Google und WhatsApp, und hast Bauchschmerzen beim Thema Privatsphäre und Sicherheit?

Darum ging es in unserem Vortragsgespräch! Wir als Bits & Bäume empfehlen freie Software für soziale Interaktionen in Zeiten von Corona (und auch generell ;) ). In dem Vortragsgespräch evaluieren wir für euch freie Video- und Schreib-Tools anhand der Kriterien Privatsphäre, Sicherheit und OpenSource und geben eine Übersicht und Empfehlungen.

- [Vortragsgespräch als Video](https://tube.tchncs.de/videos/watch/6addc421-31f0-4126-be68-fe53e540d6a8)
- [Vortragsgespräch als Audio / Podcast](https://mathias-renner.com/dl/2020-03-22-Vortragsgespraech-Video-Text-Tools.mp3)
- [Folien / Präsentation](https://www.bits-und-baeume.org/downloads/2020-03-22-Vortragsgespraech-Video-Text-Tools.pdf)
- [Tabelle mit Detail-Informationen zu Video- und Text-Tools](https://cryptpad.fr/sheet/#/2/sheet/view/yc3J+xAsidHP+QeyiAf3DdD9HUG+ytM3Ek65X5WKfnk/embed/)


Alle Materialien unterliegen der CC-BY Lizenz.

Aktuelles zu dieser und anderen Veranstaltungen & weiteren Themen immer → [im Forum](https://discourse.bits-und-baeume.org/).
