---
layout: post
title: Regelmäßige Bits&Bäume-Sprechstunde
date: 2020-05-06 21:30
author: cark
summary: Seit kurzem findet regelmäßig eine virtuelle Sprechstunde zum Thema nachhaltige IT-Nutzung statt. Zielgruppe sind insbesondere zivilgesellschaftlich engagierte Menschen.
---


Mit welchen Tricks wird ein digitales Plenum wirklich produktiv? Welche Tools sind für welchen Zweck empfehlenswert und welche nicht? Wie können wir unseren Datenaustausch vor Überwachung schützen?

Die Bits&Bäume-Gruppe Dresden bietet ab Mai 2020 eine **regelmäßige Online-Sprechstunde** rund um IT-Fragen an, die sich speziell an Menschen und Gruppen der Dresdner Zivilgesellschaft richtet, und sich für die Aspekte Nachhaltigkeit und Datenschutz interessieren. Wir können bestimmt nicht jede Frage beantworten, aber wir bemühen uns. :)

<br>
## &nbsp;&nbsp;&nbsp; → Immer Montags, 18:30 bis 19:30 Uhr.
<br>

Treffpunkt: BigBlueButton Online-Konferenzraum <https://dresden.bits-und-baeume.org/bbb>.

Kontakt: dresden{ät}bits-und-baeume.org
