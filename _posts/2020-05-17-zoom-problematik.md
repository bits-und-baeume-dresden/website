---
layout: post
title: Sicherheitslücken und unethisches Verhalten bei Zoom
date: 2020-05-22 09:00
author: Konrad Hierasimowicz
summary: In den letzten Wochen gab es massive Kritik an "Zoom". Der Beitrag fasst diese Kritik zusammen und wagt eine Empfehlung zum weiteren Umgang mit Zoom und zu den Alternativen.
---

*Ein Gastbeitrag von Konrad Hierasimowicz, Marburg*.

---

In den letzten Wochen hat die Kritik an den
Praktiken des US-amerikanischen Software-Unternehmens Zoom Video
Communications und seinen Produkten für Videokonferenzen zugenommen und
einige Institutionen, Unternehmen und sogar Staaten haben die Nutzung
einschlägiger Dienste aus dem Hause Zoom untersagt. Darunter sind das
Auswärtige Amt der Bundesrepublik Deutschland, Google und der Staat
Taiwan.[^1]

Der folgende Beitrag fasst die wichtigsten Kritikpunkte zusammen,
reflektiert die Reaktion des Unternehmens auf die Kritiken und wagt eine
vorsichtige Empfehlung für den weiteren Umgang mit den Diensten dieses
Anbieters.

Ganz allgemein fassen das Problem mit Zoom Laaff und Hegemann zusammen,
indem sie den Inhaber des Bochumer Lehrstuhls für Systemsicherheit,
Prof. Dr. Thorsten Holz zitieren: „Zoom hat sich offenbar auf die
Funktionalität seiner Software konzentriert und die Sicherheit dabei
vernachlässigt.“[^2] Dieses Problem wird auch von anderen Autor:innen
erkannt. „Aus Sicht von Anwenderinnen und Anwendern spricht \[...\]
vieles für Zoom“, konstatiert Uli Ries in seinem Beitrag für Heise
Online.[^3] „Aus Sicht von Datensicherheits- und Datenschutzfachleuten
hingegen“, so der Autor, „spricht derzeit vieles gegen die Software
beziehungsweise das Unternehmen Zoom Video.“[^4]

Im Folgenden zitiere ich die am häufigsten publizierten Kritikpunkte an
Zoom (Hervorhebung durch den Autor):

-   Zoom legt „den an sich klar definierten Begriff
    **‚Ende-zu-Ende-Verschlüsselung‘** ausgesprochen eigenwillig aus.
    US-Verbraucherschützer sprechen gar von einer Irreführung: Zoom
    verschlüssele Nutzerkommunikation laut seiner eigenen Website
    \[...\] unter bestimmten Voraussetzungen Ende-zu-Ende. Wie The
    Intercept jetzt aber berichtet, nutzt Zoom für Video- und
    Audiokonferenzen lediglich die Transportverschlüsselung per
    Transport Layer Security (TLS).“[^5]

-   „Bis Ende März \[2020 – K.H.\] schickte Zoom bei jedem Start der
    iOS-Version der Software **Daten an Facebook** \[…\]. Zu den Angaben
    gehörten die Zeitzone und Aufenthaltsort des Endgeräts, das
    verwendete Mobilfunknetz sowie eine Tracking-ID, die Werbetreibende
    zum Verfolgen von Nutzern hernehmen. In den Zoom-AGB wurden diese
    Datentransfers nicht erwähnt.“[^6]

-   „Bereits im Sommer 2019 stand Zoom in der Kritik: **Das Unternehmen
    schob Mac-Anwendern während der Installation ohne irgendeinen
    Hinweis einen voll funktionsfähigen Webserver unter.** Auch nach dem
    Entfernen der App blieb der Server auf dem System, was Apple selbst
    auf den Plan rief: Per Update der Anti-Malware-Funktion von macOS
    entfernte das Unternehmen die Komponente.“[^7]
    Sicherheitsforscher:innen kritisierten dieses Vorgehen, da es
    Hacker:innen ermögliche, schwerwiegende Eingriffe auf dem
    betroffenen System durchzuführen.

-   Aber: „Auch der derzeit verwendete Installationsmechanismus der
    Mac-Variante steht in der Kritik: Denn Zoom installiert sich auf
    Macs schon, bevor der Nutzer sein Okay gegeben hat. Mit einem
    vermeintlichen Systemdialog sollen **Root-Rechte** erlangt
    werden.“[^8] Die verheerenden Folgen dieser Sicherheitslücke bei
    einem Hackerangriff gehen weit über die Abhörgefahr hinaus: Sie
    ermöglicht unter Umständen einen direkten Zugriff auf das
    Dateisystem des betroffenen Rechners.[^9] Möglich sind damit das
    **Entwenden von Dateien oder ihre Sperrung/Löschung**.

-   Doch auch Windowsrechner sind nicht sicher. Mit einem Trick ist es
    Hacker:innen möglich, die **Kontrolle über den Rechner** zu erlangen
    und/oder die Einwahldaten (samt Passwort) zu entwenden.[^10]

Die Vorwürfe, die an die Firma Zoom Video Communications adressiert
werden, lassen sich grob in drei Kategorien zusammenfassen:

1. **Unethisches Verhalten**
(Datenweitergabe an Dritte ohne Einwilligung der Kunden, falsche
Angabe zum Verschlüsselungsverfahren, gezielte Täuschung bei der
Installationsroutine)

2. **Technisches Risiko im und nach dem Betrieb**
(Sicherheitslücken und mögliche Einbrüche ins Betriebssystem)

3. **Geringer Datenschutz der übertragenen Kommunikation**
(Performance vor Sicherheit, nicht hinreichende Verschlüsselung)

Zoom Video Communications reagierte auf die meisten dieser Vorwürfe. Als
gerichtliche Klagen[^11] hinzukamen, engagierte das Unternehmen externe
Hilfe von Sicherheitsexpert:innen.[^12] Die Leitung „kündigte \[im April
2020 – K.H.\] bereits an, in den kommenden drei Monaten sollten keine
neuen Funktionen eingeführt, sondern Schwachstellen gestopft
werden.“[^13] Als die bedeutendste Erneuerung soll „das Upgrade auf die
AES 256-bit CMG Encryption einen verbesserten Schutz für übertragende
Daten und gegen Angriffe von Außen bieten. Der neue
Verschlüsselungsstandard, der nun mit der Version 5.0 geliefert wird,
muss zunächst für das jeweilige Konto aktiviert werden, ab dem 30. Mai
soll er laut Plan der Entwickler standardmäßig implementiert sein.“

Aus technischer Sicht ist eine sichere Nutzung von Zoom nur ohne die
Ausführung der vom Browser bei der Verbindung automatisch zugesandten
Datei möglich. Das kann z. B. in Form einer ausschließlich dafür
angefertigten CD-ROM oder eines USB-Sticks mit einem bootbaren
Betriebssystem (z. B. Linux oder anderen Unixoiden) gewährleistet
werden. Bei dieser Praxis bliebe der betroffene Rechner unangetastet.
Dies erfordert jedoch stets einen Neustart des Rechners, wenn via Zoom
kommuniziert werden soll.

Auf der Ebene eines potentiellen Lauschangriffs lässt sich Folgendes
festhalten: Die Nutzung von Zoom bei der Kommunikation von Inhalten, die
weniger sensibel sind, dürfte in der Tat (abgesehen von den oben
beschriebenen technischen Risiken) nur wenige Risiken mit sich bringen.
„Sie sollten jedoch vermeiden, besonders sensible Informationen in Ihren
Zoom-Meetings preiszugeben“ konstatieren die Autor:innen des Artikels
einer österreichischen Computerzeitung.[^14] Zu einem ähnlichen Ergebnis
kommt auch das Rechenzentrum der Julius-Maximilians-Universität
Würzburg: „Wir empfehlen Zoom \[...\] für die Lehre, empfehlen
allerdings auch, kritische Informationen \[...\] nicht über Zoom zu
verbreiten.“[^15]

### Fazit:

Zoom befindet sich momentan in einer intensiven Umbauphase. Der
öffentliche Druck und die Gerichtsverfahren haben das Unternehmen
scheinbar zum „Umdenken“ gebracht. Ob tatsächlich alle kritisierten
Sicherheitsvorwürfe in hinreichendem Maße behoben werden, sollte sich
allerdings erst im Verlauf der kommenden 3 bis 4 Monate klären, also
nach den angesagten 3 Monaten Konzentration aller Ressourcen auf
Sicherheitsfragen, die Zoom angekündigt hat und einer hinreichenden Zeit
für die Reaktion von Sicherheitsspezialist:innen. Skepsis ist allerdings
nicht ganz unberechtigt, denn Zoom Video Communications hatte in der
Vergangenheit wiederholt auf Kritik halbherzig reagiert: Die Erfahrung
der letzten Monate zeigt, dass dies oft verspätet (erst nach massivem
Druck der Öffentlichkeit) geschah und die Lösungen sich nicht immer als
hinreichend bewerten ließen. Bis dahin ist die Nutzung der Dienste von
Zoom nur dann unbedenklich, wenn keine sensiblen Informationen
kommuniziert werden und der eigene Rechner, wie oben beschrieben,
geschützt wird.

---

### Alternativen (Anmerkung von Bits- und Bäume Dresden)

An Stelle Dienste von großen Konzernen einzusetzen, empfehlen wir dezentrale Alternativen auf Basis Freier Software zu benutzen. Für Videokonferenzen gibt es z.b. folgende Möglichkeiten: [Jitsi meet](https://wiki.fsfw-dresden.de/doku.php/doku/videokonferenzsysteme#jitsi-meet), [Nextcloud Talk](https://wiki.fsfw-dresden.de/doku.php/doku/videokonferenzsysteme#nextcloud_talk), [BigBlueButton](https://wiki.fsfw-dresden.de/doku.php/doku/videokonferenzsysteme#bigbluebutton).

<br>

Mehr Infos und Links gibt es in diesem [Interview](https://www.zukunftsstadt-dresden.de/interview_bits/).

---



### Quellen:

Abrams, Lawrence 2020: Zoom Lets Attackers Steal Windows Credentials,
Run Programs via UNC Links, in: BleepingComputer vom 31.3.2020, URL:
[*https://www.bleepingcomputer.com/news/security/zoom-lets-attackers-steal-windows-credentials-run-programs-via-unc-links/*](https://www.bleepingcomputer.com/news/security/zoom-lets-attackers-steal-windows-credentials-run-programs-via-unc-links/)

Bernecke, Jan 2020: Zoom 5.0: Anbieter verspricht höhere
Sicherheitsstandards und mehr Datenschutz, in: entwickler.de vom
23.4.2020, URL:
[*https://entwickler.de/online/tools/zoom-5-0-579932496.html*](https://entwickler.de/online/tools/zoom-5-0-579932496.html)

Bünte, Oliver 2020: Sicherheitsmängel: Google blockiert Zoom auf
Arbeitsrechnern, in: Heise online vom 9.4.2020, URL:
[*https://www.heise.de/newsticker/meldung/Sicherheitsmaengel-Google-blockiert-Zoom-auf-Arbeitsrechnern-4700339.html*](https://www.heise.de/newsticker/meldung/Sicherheitsmaengel-Google-blockiert-Zoom-auf-Arbeitsrechnern-4700339.html)

DPA 2020 (1): New Yorker Staatsanwältin prüft Datenschutz bei
Konferenz-App Zoom, in: Heise online vom 1.4.2020, URL:
[*https://www.heise.de/newsticker/meldung/New-Yorker-Staatsanwaeltin-prueft-Datenschutz-bei-Konferenz-App-Zoom-4694352.html*](https://www.heise.de/newsticker/meldung/New-Yorker-Staatsanwaeltin-prueft-Datenschutz-bei-Konferenz-App-Zoom-4694352.html)

DPA 2020 (2): Zoom ruft Ex-Sicherheitschef von Facebook zur Hilfe, in:
FOCUS Money online vom 8.4.2020, URL:
[*https://www.focus.de/finanzen/boerse/wirtschaftsticker/unternehmen-zoom-ruft-ex-sicherheitschef-von-facebook-zur-hilfe\_id\_11865461.html*](https://www.focus.de/finanzen/boerse/wirtschaftsticker/unternehmen-zoom-ruft-ex-sicherheitschef-von-facebook-zur-hilfe_id_11865461.html)

Funken, Matthias u. A. 2020: Zoom - Stellungnahme zu Schwachstellen,
URL:
[*https://www.rz.uni-wuerzburg.de/dienste/lehre-digital/zoom/zoom-stellungnahme/*](https://www.rz.uni-wuerzburg.de/dienste/lehre-digital/zoom/zoom-stellungnahme/)

Goodin, Dan 2019: Silent Mac update nukes dangerous webserver installed
by Zoom, in: Ars Technica vom 11.7.2019, URL:
[*https://arstechnica.com/information-technology/2019/07/silent-mac-update-nukes-dangerous-webserver-installed-by-zoom*](https://arstechnica.com/information-technology/2019/07/silent-mac-update-nukes-dangerous-webserver-installed-by-zoom)

Laaff, Meike 2020: Ok, Zoomer, in: ZEIT Online vom 31.3.2020, URL:
[*https://www.zeit.de/digital/2020-03/videokonferenzen-zoom-app-homeoffice-quarantaene-coronavirus/komplettansicht*](https://www.zeit.de/digital/2020-03/videokonferenzen-zoom-app-homeoffice-quarantaene-coronavirus/komplettansicht)

Laaff, Meike und Lisa Hegemann 2020: Zoom. Unter Beobachtung, in: ZEIT
Online vom 3.4.2020, URL:
[*https://www.zeit.de/digital/datenschutz/2020-04/zoom-videokonferenzdienst-probleme-mac-windows-computer/komplettansicht*](https://www.zeit.de/digital/datenschutz/2020-04/zoom-videokonferenzdienst-probleme-mac-windows-computer/komplettansicht)

Lee, Micah und Yael Grauer 2020: Zoom Meetings Aren't End-to-End
Encrypted, Despite Misleading Marketing, in: The Intercept vom
31.3.2020, URL:
[*https://theintercept.com/2020/03/31/zoom-meeting-encryption*](https://theintercept.com/2020/03/31/zoom-meeting-encryption)

Martin, Jim und Julia Krokoszinski 2020: 9 Zoom-Tipps für mehr
Privatsphäre und Sicherheit, in: Computerwelt vom 24.4.2020, URL:
[*https://computerwelt.at/knowhow/9-zoom-tipps-fuer-mehr-privatsphaere-und-sicherheit/*](https://computerwelt.at/knowhow/9-zoom-tipps-fuer-mehr-privatsphaere-und-sicherheit/)

Neuerer, Dietmar und Moritz Koch: Auswärtiges Amt untersagt Nutzung von
Zoom auf dienstlichen Geräten, in: Handelsblatt vom 8.4.2020, URL:
[*https://www.handelsblatt.com/technik/it-internet/it-sicherheit-auswaertiges-amt-untersagt-nutzung-von-zoom-auf-dienstlichen-geraeten/25726922.html?ticket=ST-862674-hPTgviSb4aZEzR6g7XL9-ap3*](https://www.handelsblatt.com/technik/it-internet/it-sicherheit-auswaertiges-amt-untersagt-nutzung-von-zoom-auf-dienstlichen-geraeten/25726922.html?ticket=ST-862674-hPTgviSb4aZEzR6g7XL9-ap3)

Ries, Uli 2020: Videokonferenz-Software: Ist Zoom ein
Sicherheitsalptraum? In: Heise online 04/2020, URL:
[*https://www.heise.de/security/meldung/Videokonferenz-Software-Ist-Zoom-ein-Sicherheitsalptraum-4695000.html*](https://www.heise.de/security/meldung/Videokonferenz-Software-Ist-Zoom-ein-Sicherheitsalptraum-4695000.html)

Scherschel, Fabian A. 2020: Meeting-Zwang: Mac-Kameras lassen sich über
Zoom-Sicherheitslücke anschalten, in: Heise online 07/2019, URL:
[*https://www.heise.de/security/meldung/Meeting-Zwang-Mac-Kameras-lassen-sich-ueber-Zoom-Sicherheitsluecke-anschalten-4466205.html*](https://www.heise.de/security/meldung/Meeting-Zwang-Mac-Kameras-lassen-sich-ueber-Zoom-Sicherheitsluecke-anschalten-4466205.html)

Soni, Jitendra und Mike Moore: Germany bans Zoom for official use, in:
techradar.com vom 9.4.2020, URL:
[*https://www.techradar.com/news/zoom-banned-for-official-use-in-taiwan*](https://www.techradar.com/news/zoom-banned-for-official-use-in-taiwan)

techware01 2020: Das Problem mit dem Datenschutz am Beispiel von Zoom,
in: digitale-Lehre-und-Datenschutz vom 27.3.2020, URL:
[*https://github.com/techware01/digitale-Lehre-und-Datenschutz/blob/master/zoom.md*](https://github.com/techware01/digitale-Lehre-und-Datenschutz/blob/master/zoom.md)

[^1]: Bünte 2020, Neuerer und Koch 2020 sowie Soni und Moore 2020.

[^2]: Laaff und Hegemann 2020.

[^3]: Ries 2020.

[^4]: Ebd.

[^5]: Ebd. Vgl. Lee und Grauer 2020.

[^6]: Ebd.

[^7]: Ebd. Vgl. Laaff 2020 und Goodin 2019.

[^8]: Ebd. Ferner beschreibt der Autor, wie ein potentieller Angreifer
    „auf dem Zoom eingeräumten Zugriff auf Kamera und Mikrofon reiten
    und die Komponenten jederzeit ohne Rückfrage aktivieren \[kann\].
    Wahlweise lassen sich über die Schwachstelle auch beliebige
    Zoom-Konversationen mitschneiden.“ Berichte Darüber gab es bereits
    2019, siehe Scherschel 2019.

[^9]: Siehe techware01 2020.

[^10]: Ebd. Vgl. Abrams 2020.

[^11]: DPA 2020 (1).

[^12]: DPA 2020 (2).

[^13]: Ebd.

[^14]: Martin und Krokoszinski 2020.

[^15]: Funken u. A. 2020.
