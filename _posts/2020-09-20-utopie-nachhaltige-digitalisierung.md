---
layout: post
title: "Fragmente einer Utopie: Wie könnte eine nachhaltig digitalisierte Gesellschaft 2030 aussehen?"
date: 2020-09-28 22:00
author: cark, rave
summary: Inspiriert durch die 11 Forderungen der Bits- und Bäume-Bewegung wird eine Utopie beschrieben, wie Deutschland 2030 aussehen könnte, wenn die Digitalisierung sich primär am Nachhaltigkeitsprinzip und den Bedürfnissen der Menschen ausrichtet.

---

## Fragmente einer Utopie: Wie könnte eine nachhaltig digitalisierte Gesellschaft 2030 aussehen?


Dieser Text bildete den inhaltlichen Kern des gleichnamigen Vortrags auf den [Datenspuren 2020](https://datenspuren.de/2020/).

Video:

- [https://media.ccc.de/v/...](https://media.ccc.de/v/ds20-11323-fragmente_einer_utopie_wie_konnte_eine_nachhaltig_digitalisierte_gesellschaft_2030_aussehen)
- [https://www.youtube.com/watch?v=d718STMgu0A](https://www.youtube.com/watch?v=d718STMgu0A)


### Rahmenhandlung: Perspektive 2030

Wir blicken zehn Jahre zurück. Im Jahr 2020 hatten viele Menschen auf Grund ausgefallener Veranstaltungen und Reisen unerwartet Zeit, Zeit um nachzudenken. Außerdem hatte die Pademie und ihre Eindämmung die Einsicht verbreitet, dass sich Probleme durch vernüftiges und entschlossenes Handeln in den Griff bekommen lassen, während sowohl Ignorieren als auch Hysterie die gesamtgesellschaftliche Lage verschlimmern. Und so begab es sich, dass in der Folgezeit auch andere komplexe Probleme mit einem noch am Anfang des Jahrzehntes für völlig unrealistisch ja sogar utopisch gehaltenem Erfolg gelöst wurden, So hat sich zum Beispiel die Digitalisierung von einem Brandbeschleuniger verletzter Nachhaltigkeitsprinzipien (Zitat [WBGU-Bericht 2019](https://www.wbgu.de/de/publikationen/publikation/unsere-gemeinsame-digitale-zukunft)) und bestehender sozialer Misstände zu einem Prozess gewandelt, der zur Lebensqualität aller Menschen beiträgt. Das wird in vielen Bereichen deutlich.

### Bildung

Früher war der Bildungserfolg in hohem Maße herkunftsabhängig, es gab ein schlechtes Verhältnis von aufgewendeter Zeit und **echten Lernerfolgen**. Das äußerte sich in ingesamt unzureichender **Vermittlung von demokratierelevanten Werten und Fähigkeiten**, fehlenden harten und weichen Kompetenzen und nicht zuletzt im viel beklagten Fachkräftemangel.

Heute läuft vieles deutlich besser. **Exzellentes Lehrmaterial unter freien Lizenzen**, kooperative Qualitätssicherung und Weiterentwicklung, z. B. Arbeitsblätter, Texte, Übungs- und Prüfungsaufgaben, Audio-/Videomaterial, Lernsoftware. Auch sehr gut auffindbar und auswählbar. Material steht für alle Altersgruppen (d. h. nicht nur für Schulbildung), Schwierigkeitsniveaus und Inhalte zur Verfügung, von Hand-Auge-Koordinationsübungen in der Vorschule über Bruchrechnen bis zur Pharmazielehre für die Altenpflegeausbildung.

Auch die Aus- und Weiterbildung von Lehrkräften profitiert enorm davon. Zuverlässige Handschrifterkennung und inhaltliches Textverständnis von Software ermöglicht die Korrektur von Übungen und Tests, Lernende bekommen mehr und besseres Feedback und haben dadurch mehr Motivation. Das spart Ressourcen von Lehrkräften, die sich dann besser auf das Beantworten von Fragen und individuelle Förderung konzentrieren können. Auch Lehrkräfte und Schulpsycholog:innen/Sozialarbeiter:innen werden durch laufendes Feedback mit digitalen Tools unterstützt, was Probleme früher erkennen lässt und Bürokratie/Zuständigkeitsfragen abbaut.

Lernende können in Videokonferenzen unkompliziert Lerngruppen bilden und sich gegenseitig unterstützen.

Der Anfang der 20er Jahre noch vorherrschende Mangel an Geräten und Netzkapazitäten wurde behoben, durch die Ertüchtigung von älterer Hardware mittels freier Software und durch effizientere Nutzung bestehender Übertragungskapazitäten (Datensparsamkeit), sowie durch Freifunk-Initiativen


### Individuelle Selbstbestimmung und Sucht

Anfang der 20er Jahre setzte sich die Erkenntnis durch, dass die damaligen **"soziale Netzwerke" diesen Namen in den seltensten Fällen verdient hatten**. Folgen sowohl für die persönliche psychische Gesundheit, als auch die systematische Bevorzugung von negativen Emotionen, zeigten sich zunehmend. Was Psycholog:innen und Soziolog:innen längst erforscht hatten, wurde vor allem durch die Pandemie ab 2020 deutlich sichtbar, da Kontaktbeschränkungen die soziale Sphäre noch stärker dorthin verlagerten. Die eingesetzten Mechanismen waren perfide: Durch Strategien zur Screentime-Maximization wurden Nutzer:innen bewusst lange auf Plattformen gehalten, um mehr Werbeeinnahmen zu generieren und Nutzungsverhalten auszuwerten. Die Nutzer:innen waren von selbstbestimmten Individuen zu gesichtslosen "Usern" geworden. Hassrede und Fake News, die als Nebeneffekt davon auftauchten, waren schwer zu kontrollieren. Die Menschen begannen, diese Mechanismen zu hinterfragen.

Aus einer Welle von **Dopamin-Detox** ("Dopamin-Fasten") und dem Boykott solcher Plattformen resultierten neue Angebote, die auf "Screen-Time-Optimization" statt "-Maximization" abzielen und ihr psychologisches Wissen nicht zur Erzeugung von Sucht und Abhängigkeit nutzen, sondern echte langfristige Wertschätzung durch Kund:innen generieren.

Features wie Autoplay und Vorschläge, die man nicht ablehnen kann, sind heute weitgehend verschwunden. Und ersetzt durch Assistenten, die für die aktuelle Stimmungslage passende, unterhaltsame und interessante Inhalte für eine angemessene Zeitspanne bereitstellen. Das Resultat sind **selbstbestimmte Nutzungsmuster und dadurch zufriedenere Nutzer:innen**, deren Wertschätzung für gute Inhalte sich auch in angemessene Zahlungsbereitschaft umsetzt.

Heute, im Jahr 2030, ist es eine Selbstverständlichkeit, dass alle Nutzer:innen transparent, nachvollziehbar und sicheren Lese- und Löschzugriff auf die über Sie gespeicherten Daten bei Unternehmen und anderen Körperschaften haben. **Datensparsamkeit** ist auf Grund konsequenter Gesetze und beständiger Nachfrage zu einem **Leitprinzip der digitalen Produktentwicklung** geworden.


### Demokratische Teilhabe und Verwaltung

Am Anfang stand die zentrale Erkenntnis: **Regieren ist im Grunde wie Software-Entwicklung** – und zwar für eine massiv parallele Prozessor-Architektur, mit ca. 80 Millionen Kernen (wenn man von Deutschland ausgeht), die alle von mehr oder weniger starken Bugs betroffen sind. Die Software der Gesellschaft sind Gesetze und Verordnungen und das meist unsichtbare umgebende Geflecht aus sozialen Normen.


Auf Basis dieser Erkenntnis konnten einige Innovationen der Software-Entwicklung übernommen werden:

**Dokumentation:** Zu jedem Zeitpunkt muss klar sein, welche Regeln gelten in welcher Situation. Was ist ihre Intention. Wann und von wem wurden sie implementiert. Formale Tests belegen die Widerspruchsfreiheit verschiedener Regeln und beugen so z. B. offensichtlichen Gesetzeslücken vor.

**Fehlerkultur und kontinuierliche Weiterentwicklung:** Bugs sind in der Softwareentwicklung unvermeidlich. Das Äquivalent im politischen Prozess heißt "Gesetzeslücke bzw. –fehler". Außerdem ändern sich die Anforderungen an die Software ständig und die Gesetzgebung hinkt hinterher ("Regelugierungslücke"). In der klassischen Softwareentwicklung gibt es dafür ausgefuchste Mechanismen zum sog. Issue-Tracking. Also zum Melden und Dokumentieren von Problemen und Feature-Wünschen. Diese Plattform dient dann auch für den Diskussionsprozess über die beste Lösungsmethode.

Für die demokratische Teilhabe wurde nach diesem Vorbild eine **Missstandsmeldestelle** eingerichtet. Die steht allen wahlberechtigten Bürger:innen offen. Dort können alle Prombleme eingestellt und klassifiziert werden. Durch Software-Unterstützung und hauptamtliche Moderator:innen ist schnell klar, ob das Problem schon bekannt ist (Duplikat), mit welchen anderen es zusammenhängt, und wer dafür zuständig ist. Außerdem ist der **Fortgang der Problemlösung für alle transparent** sichtbar. Eine enorme Verbesserung gegenüber früher, wo legitime Anliegen oft in intransparenten Strukturen versackten

Viele Probleme sind naturgemäß sehr subjektiv und bedürfen eines **demokratischen Aushandlungsprozesses**. Der hat zu Beginn des Jahrzehntes ziemlich schlecht funktioniert (Diskussionskultur). Es wurde viel aneinander vorbeigeredet und bewusst provoziert. Eine sachliche Abwägung von Argumenten wurde oft durch hochemotionalisierte Empörung überlagert. Die Pandemie hat deutlich gemacht, dass es besser für alle ist, wenn sich **nicht die lautesten sondern die überzeugendsten Standpunkte im Diskurs durchsetzen**. Die Art wie seitdem Diskussionen im Netz ablaufen ist eine ganz andere. Das wahren einer **konstruktiven Diskussionskultur** wird als essentielle Voraussetzung erkannt und **mit technischer Unterstützung umgesetzt**. z. B. sind einige Diskussionsplattformen nicht mehr chronologisch sondern inhaltlich nach Argumenten und Gegenargumenten sortiert. Statt des Prinzips "Es ist zwar schon alles gesagt, aber noch nicht von jedem", werden vorhandene Diskussionsbeiträge kollaborativ verbessert und ergänzt. **Thematische Abschweifungen werden separiert und Beleidigungen semi-automatisch gelöscht** - es gibt ja durch Algorithmen unterstützte hauptamtliche Moderator:innen!

Faktenbasierte Diskussionen sind um ein vielfaches leichter geworden, seit die technischen Möglichkeiten zur **Verifikation und Authentifizierung** breite Anwendung finden. Unvorstellbar ist heute, dass noch 2020 eine Firma namens Wirecard Bilanzbetrug in Milliardenhöhe begehen konnte weil Kontoauszüge mit Bildbearbeitungssoftware manipuliert wurden – und das Jahrzehnte nach der Erfindung der kryptografischer Signaturen. Heute **lernt jedes Kind in der Schule neben Verschlüsselung auch wie man digital signiert** und Signaturen und Zertifikate prüft.

Nützlich ist auch die große **Blockchain-basierte Verifikationsplattform**, die allen zur Verfügung steht. Damit können alle zweifelsfrei überprüfen, ob ein Dokument, ein Bild ein Zitat oder ein Video zu einem gegebenen Zeitpunkt in der später behaupteten Form und Urheberschaft schon existiert hat. Technisch sind "Deep-Fakes", also z. B. täuschend echt simulierte Videos mit Falschaussagen von Politiker:innen zwar möglich, aber bedeutungslos, weil ohne Verifikation ihnen niemand glaubt.


Die drei Milliarden Euro aus dem dritten Pandemie-Konjunkturpaket, die **in die digitale Weiterentwicklung der Demokratie investiert** wurde, war im Nachhinein ziemlich gut angelegt.


### Bezahlung

Fast unvorstellbar mutet heute die Bezahlsituation von vor 10 Jahren an. Damals gab es eigentlich nur schlechte Lösungen. Digitales Bezahlen war entweder umständlich, mit Sicherheitsrisiken verbunden, oder diente zur **Ausleuchtung des eigenen Konsumverhaltens durch gewinnorientierte Unterehmen**. Bezahldienstleister verdienten sich trotz geringem Mehrwert für die Gesellschaft eine goldene Nase. Inzwischen ist Bezahlen als öffentliche Infrastruktur anerkannt. Die Bezahlung von Kleinstbeträgen ist mit Hilfe von neuen Cryptowährungen unkompliziert, sicher, anonym und schnell. Und erst das hat ganz neue Geschäftsmodelle im Internet ermöglicht. Würde man jetzt vorschlagen, wieder zur **archaischen Werbefinanzierung** zurückzukehren, wo die eigentlichen Inhalte vor lauter nerviger Anzeigen kaum zu erkennen waren, könnte man sich vor Hohn und Spott kaum retten.

Dadurch, dass die Quellenseite des Bezahlvorgangs immer anonym bleibt, die Empfänger:innen-Seite hingegen leicht ermittelt werden kann, sind Geldwäsche und Steuerhinterziehung zumindest über dieses Medium nicht möglich.

Bargeld existiert weiterhin, allerdings haben sich die Bedenken bzgl. dessen Abschaffung aus rechtlicher Sicht erübrigt, weil ausreichend anonymisierter Zahlungsverkehr jetzt zuverlässig gewährleistet ist. So wie Vinyl-Platten, erfüllt es heute hauptsächlich anfassbaren und nostalgischen Wert.


### Medien

Das Gefährungspotenzial durch Falschmeldungen zu Beginn des Jahrzehnts hat dazu geführt, dass Medien, die für sich das Prädikat **Qualitätsjournalismus beanspruchen, zu konsequenter Angabe aller relevanter Quellen** übergegangen sind. In jedem digital veröffentlichten Artikel lassen sich Fußnoten mit Referenzen optional einschalten, in jedem Video ist eine URL mit Quellenangaben enthalten. Dies hat dazu geführt, dass sich eine **diskursive Erwartungshaltung etabliert** hat. Wer heutzutage eine Behauptung in den Raum stellt, ohne eine solide nachprüfbare Quellenangabe, macht sich eigentlich nur noch lächerlich bzw. wird schlicht ignoriert. Die Reichweite kontrafaktischer Publizistik z. B. über den Klimawandel und absurde Verschwörungsmythen wie die Chemtrails-Erzählung beschränkt sich inzwischen auf einen kleinen Kreis desorientierter Menschen ohne gesellschaftliche Relevanz.

Die Medienlandschaft hat sich ebenfalls stark gewandelt. Die reformierten öffentlich-rechtlichen Medien bieten zwar nach wie vor eine solide Grundversorgung, aber das, was früher mal große Verlagshäuser mit Macht und Deutungshoheit waren, gibt es so nicht mehr. Stattdessen gibt es eine **lebendige Szene freier Journalist:innen**, Redaktionen und Korrespondent:innen die zu Ihrem Gebiet und in Ihrer Region recherchieren und publizieren.
Mediennutzer:innen suchen mit Hilfe von Softwareassistenten die Inhalte, die sie interessieren könnten und bezahlen entweder pauschal oder inhaltsbezogen.

Werbung hat als Finanzierungsquelle nur noch einen sehr geringen Stellenwert. Irgendwann war klar, dass Ad-Blocker einfach zu gut sind, so dass sich faire inhaltsbezogene Bezahlmodelle durchgesetzt haben. Dadurch ist der Anreiz zu Klickbaiting und Sensationsjournalismus deutlich kleiner geworden.


### Kultur und Spiele

Kulturschaffende werden nicht mehr nach dem alten System der Verlags- und Aufführungsrechte wie GEMA, **monopolistischen Verwertungskonzernen wie Youtube und Spotify** entlohnt oder durch einmalige Förderungspakete kurzfristig gestützt, sondern erhalten planbare Unterstützung aus der Gesellschaft durch **solidarische Abo-Modelle**, wie es die Firma Patreon Ende der 10er Jahre etabliert hat. Dieses Modell konnte sich durchsetzen. Öffentliche Förderung für besonders wertvolle Inhalte - wobei "wertvoll" demokratisch ausgehandelt wird - wird zusätzlich ausgebaut. Durch neue Techniken zur Verbreitung von Musik und Film wurden Streaming-Plattformen wie Spotify durch viele kleine, interoperable Dienste verdrängt. Interoperabel heißt, dass es gesetzlich vorgeschriebene Schnittstellen zum Daten- und Vergütungsaustausch gibt.

Computerspiele haben sich in den letzten Jahren noch weiter in den Mittelpunkt der Gesellschaft bewegt. Auch deren Auswirkung auf die psychische Gesundheit hinsichtlich möglicher Suchtpotentiale wird offen behandelt. Öffentliche Stellen wie die Bundeszentrale für gesundheitliche Aufklärung (BzgA) und gesellschaftliche Initiativen nach dem Vorbild der Verbraucher:innenzentralen geben wissenschaftlich fundierte Einschätzungen zu Spielen heraus, die allgemein angesehen sind. Die Praxis der **In-App- oder In-Game-Käufe**, die Spieler:innen im Dopaminrausch Angebote machen, die sie schwer ablehnen können, wird schlecht bewertet und **gesellschaftlich sanktioniert**, ähnlich dem in den 10er Jahren aufgekommene Nutri-Score für Nahrungsmittel. Dadurch werden Spieleentwickler:innen angeregt, bereits bei der Entwicklung Design-Patterns zur Suchtvermeidung oder direkte Hinweise an Spieler:innen einzubauen. Einzelne Mittvierziger kennen noch den frühen Vorboten "Anno 1404" aus dem Jahr 2009, das Spieler:innen alle zwei Stunden auf eine Pause hingewiesen hatte.


### Industrielle Produktion und Lieferketten

Nach Überwindung der Pandemie am Anfang des Jahrzehnts trat die eigentliche existenzielle Krise wieder in den Vordergrund: die ökologisch und sozial destruktiven Produktions- und Konsummuster der globalisierten Weltwirtschaft. Und auch wenn diesbezüglich heute noch viel im Argen liegt und die Menschheit noch nicht vollständig über den Berg ist: Durch den **klugen Einsatz digitaler Technologien** wurden enorme Fortschritte erreicht. Zunächst mal wird heute trotz gestiegener Weltbevökerung **deutlich weniger produziert** als früher, dafür werden Güter länger genutzt und ggf. repariert. Möglich ist das, weil z. B. durch **3D-Druck und quelloffene Konstruktionspläne Ersatzteile zielgerichtet nach Bedarf bereitgestellt** werden können. Außerdem sind die Kund:innen schlicht nicht mehr bereit, Produkte zu kaufen, die nach zwei Jahren schrottreif sind und der Druck von **authentischen Produktbewertungen** zwingt die Hersteller zu entsprechender Haltbarkeit.

Seit ein paar Jahren muss zu jedem neuen Produkt übrigens auch ein komplettes **digitales Lieferkettenzertifikat** veröffentlicht werden. D.h. egal, ob Joghurtbecher oder Smartphone: Die komplette Lieferkette kann transparent nachvollzogen werden, inklusive Herkunftsland, Transportwegen, CO2-Bilanz, ökologischem Rucksack etc. Aus diesen Daten lässt sich dann zusammen mit Infos zu Arbeitsbedingungen, z. B. von Gewerkschaften, die Nachhaltigkeit eines Produktes ziemlich gut ermitteln. Aktuell ist in der Debatte, diesen Wert in den Mehrwertsteuersatz einzubeziehen.


### Verkehr

Früher war es ja noch so, dass Leute sich ein Auto gekauft haben, das dann fast nur rumstand. Jetzt geht es einfach um Zugang zu Mobilität. Die meisten Leute haben ihr persönliches Auto inzwischen abgegeben und sparen eine Menge Geld, Zeit und Ärger. Wer von A nach B will - und dabei tatsächlich auf ein Auto angewiesen ist -, bucht ein sogenanntes *Autonom* über eine App oder einen Sprachassistenten und fertig. Einstellen kann man noch ob Mitfahrende und Umwege akzeptabel sind und wie flexibel die Abfahrtszeit ist, danach richtet sich dann der Preis. Dadurch wurde die Grenze zwischen Individual- und öffentlichem Verkehr sehr fließend. Und das Leben auf dem Land ist wieder viel attraktiver geworden, gerade für ältere Menschen.

Weil die Fahrzeuge über einheitliche Schnittstellen miteinander kommunizieren, schließen Sie sich auf größeren Straßen flexibel zu einer Art Ad-hoc-Zug zusammen (Platooning), was ihren Luftwiderstand deutlich senkt und für ein gleichmäßigeres Fahren sorgt. Langsame Fahrzeuge wie LKW und Traktoren bekommen automatisch verhandelte Boni von den anderen Fahrzeugen, wenn Sie kurz rechts ran fahren und die schnelleren vorbeilassen. Staus und Unfälle gibt es nur noch äußerst selten. Die Fahrzeit lässt sich zum Schlafen, Lesen oder Arbeiten benutzen. Ich habe z. B. fünf Paar Socken gestrickt während ich letzten Winter regelmäßig von Bautzen nach Senftenberg gependelt bin.
Früher sind ja in Deutschland im Schnitt 10 Menschen am Tag durch Autounfälle ums Leben gekommen, das muss man sich mal vorstellen. Jetzt haben wir diese Zahl nicht mal in einem Jahr.

Der klassische Nahverkehr hat massiv an Attraktivität gewonnen. Durch eine öffentlich entwickelte, hochwertige "Mobil-App" ist der unübersichtliche Tarifdschungel überwunden. Taktung von Bahn und Bus kann bedarfsgerecht gesteuert werden, wodurch sich Wartezeiten stark verringert haben und das Netz dichter geworden ist. In Ballungsräumen ist es **inzwischen völlig absurd, ein Auto zu benutzen**, solange man nichts sperriges zu transportieren hat.
Im Fernverkehr kommen überfüllte Züge fast nicht mehr vor, weil es zuverlässige Echtzeit-Prognosesysteme gibt und dann gezielt nachgesteuert wird. Einerseits durch die Verstärkung von Zügen, andererseits mit Rabatt-Angeboten für Menschen, die ihre Fahrt auf einen Zeitpunkt geringerer Auslastung verschieben.

Das meistbenutzte Fortbewegungsmittel ist jedoch das Fahrrad, zumindest in den Städten. Dessen Siegeszug war - angekurbelt durch den Fahrrad-Hype der Covid19-Pandemie - nicht mehr aufzuhalten. Sichere Fahrradparkhäuser und zuverlässig und anonym funktionierende Bike-Sharing Plattformen fanden im Lauf der 20er Jahre so großen Zulauf, dass selbst die **Lobby der Automobilindustrie machtlos** war. Intelligtente Ampelschaltungen, die Fahrradfahrenden nach Möglichkeit Vorrang geben, sowie eine rigoros ausgebaute Radinfrastruktur sind die Folge. Das Mehr an Bewegung findet sich positiv im Gesundheitszustand der Bevölkerung wieder.

Diese **komischen E-Roller sind Anfang der 20er übrigens ziemlich schnell wieder verschwunden**. Dazu hat auch beigetragen, dass einfach immer mehr Leute lustige Sticker auf ihren QR-Code klebten...


### Handel und Logistik

Kennt Ihr noch die Zeit, als alle größeren Innenstädte und Einkaufszentren gleich aussahen? Die gleichen Marken, die gleiche, an Großraumgefängnisse erinnernde Architektur. Das hat sich gewandelt. **Shopping als Zeitvertreib** hat sich irgendwie überlebt. Einkaufen läuft größtenteils bedürfnis-orientiert und online. Ein bisschen gewöhnungsbedürftig war die Sache mit den 3D-Scannern, aber jetzt möchte ich es nicht mehr missen.

Einmal im Jahr lasse ich mich vermessen und basierend darauf probiere ich Klamotten immer virtuell an. Dann sehe ich in der Live-Animation wie es sitzt und wie z. B. Falten fallen. Geradezu archaisch erscheint mir dagegen das ständige Wechseln zwischen Umkleidekabine und Ladenbereich. Knapp die Hälfte des Klamottengeschäfts ist übrigens inzwischen Gebrauchtware, von privat zu privat, durch authentische Fotos und 3D-Modelle kann man die Qualität ziemlich gut einschätzen und es ist eher selten, dass man was zurückschicken muss.

Der **Versandhandel läuft über wiederverwendbare Boxen**. Diese Boxen wissen, wem sie aktuell gehören, wer sie  öffnen oder  mitnehmen darf und wo sie gerade sind. Dadurch können Sie einfach an vielen Stellen abgegeben werden. Sinnloses Verpackungsmaterial fällt fast gar nicht mehr an.

Der **freigewordene Platz in den Innenstädten** hat die Lebensqualität massiv verbessert. Die **Mieten sind gesunken**, und es gibt jetzt viel Räume für Kultur, Zivilgesellschaft und Sport.



### Rohstoffe und Recycling

Die Einstellung zum Verbrauch von Ressourcen hat sich geändert, denn er ist heute viel besser sichtbar. Die meisten Geräte besitzen Funktionen zum Aufzeichnen und Auswerten ihres Verbrauchs, um den **Verbraucher:innen direkte Rückmeldung zur konkreten Auswirkung ihrer Nutzung** zu geben. So haben etwa Wasserhähne in der Küche eine kleine Anzeige zum Warmwasserverbrauch und laden dadurch ständig unbewusst zum Sparen ein. Vor allem auch stromhungrige Elektrogeräte wie Klimaanlagen werden dadurch mit mehr Bedacht benutzt.
Der sozial-ökologische Rucksack eines jeden Produkts, also die verwendeten Materialien, deren Transportwege, Umstände der Förderung, Trennbarkeit bei der Entsorgung und so weiter werden strikt transparent aufgeführt. Es gibt Apps, die durch Bilderkennung **jedes Produkt und jeden Gegenstand in ein leicht verständliches Raster von Nachhaltigkeitskriterien einordnen**. Dadurch entsteht Druck von Konsument:innenseite, Produkte möglichst nachhaltig zu gestalten, und Produkte mit unvermeidbar "schwerem Rucksack" werden entsprechend mit mehr Respekt benutzt. Die Bilderkennung gibt außerdem Hinweise zur korrekten Entsorgung von Produkten und deren Verpackungen. Derzeit sind intelligente Mülltonnen im Probelauf, die piepen wenn ein offensichtlich falscher Gegenstand eingeworfen werden soll. Gerüchteweisehaben die Testkandidat:innen seitdem ein besseres Verhältnis zu ihren Nachbarn...

Was digitale Geräte betrifft, so tritt deren Entsorgung in den Hintergrund. Die vollständige Entsorgung eines Geräts muss nur noch selten passieren, da **bessere Reparierbarkeit, Verfügbarkeit von Ersatzteilen und sichergestellten Softwareupdates seine Nutzungsdauer massiv erhöht**. Unterstützt wird dies durch eine gesetzliche Verpflichtung, dass neu entwickelte End-User-Software grundsätzlich auch auf älterer Hardware laufen muss. Tut sie das nicht, muss der Hersteller dafür einen plausiblen Grund angeben.
Dadurch sind viel weniger neue Geräte im Umlauf und der Lifestyle-Faktor der glänzend neuen Produkte ist einer Vintage-Ästhetik gewichen. So wie es Ende der 10er Jahre hip wurde, ein möglichst altes Fahrrad zu fahren, ist es heute angesagt, ein individualisiert zerschrammtes und durch hobby-Hacks gepimptes altes Smartphone zu benutzen. Originale Nokia 3310 werden zu astronomischen Preisen gehandelt.


### Gesundheit

Das Gesundheitssystem hat sich radikal gewandelt. Auch durch die Lehren aus der Pandemie werden viele Aufgaben in der ärzlichen Behandlung und in der **Pflege heute digital unterstützt, um das Personal zu entlasten** und Vorgänge zu verbessern. So ist es etwa für viele alte Menschen üblich, sich von Pflegeassistent, früher abwertend Pflegeroboter genannt, waschen zu lassen. Viele finden das sogar angenehmer, da dann nicht andere Menschen so tief in ihre körperliche Intimität eingreifen und ihnen mehr Souveränität bleibt. Die gewonnene Zeit hilft den Pflegekräften, **individueller auf die Pflegebedürftigen einzugehen**. Daten, die der Pflegeassistent bei seiner Arbeit erfasst, können früh auf mögliche körperliche und psychische Krankheiten hinweisen.
Auch in Krankenhäusern und bei niedergelassenen Ärzt:innen hat die Digitalisierung nach anfänglichen Problemen große Entlastungen mit sich gebracht. Falsch übertragene Daten, verlorene Akten oder gar Verwechslung von Personen sind heute nur noch Stoff für Filme. Die elektronische Patientenakte hatte einen miserablen Start. Als dann aber endlich **Expert:innen aus der Zivilgesellschaft in die Entwicklung einbezogen** worden, ist daraus ein sicheres und selbstbestimmt nutzbares System geworden. Auch ältere Menschen haben durch gut gestaltete Assistenzsysteme die Möglichkeit, souverän damit umzugehen. Durch entsprechend verfügbare Daten ist auch die Kenntnis von unerwarteten Wechselwirkungen von Medikamenten stark verbessert, was viel Leid verhindert hat.

Ferndiagnose durch virtuelle Arztbesuche und eine in Grenzen automatisierte Bewertung von körperlichen Beschwerden (z. B. Analyse von Leberflecken) entlastet Ärzt:innen sowie Patient:innen. Durch derartige Präventionsmaßnahmen können viele Probleme gelöst werden, bevor sie wirklich schlimm sind.

Die allgemeine körperliche und psychische Gesundheit der Bevölkerung hat sich verbessert. Es gibt seit längerem **wissenschaftlich geprüfte Apps, die zur Aufklärung für eine gesunde Lebensweise** beitragen. Sie kann per Bilderkennung zu den meisten Nahrungsmitteln Auskunft zu verstecktem Zucker- oder Salzgehalt geben. Wer möchte, kann auf einfache Art und Weise ein Ernährungstagebuch führen. Unterstützung zum verantwortungsvollen Umgang mit Rauschmitteln steht ohne moralischen Zeigefinger zur Verfügung.


### Intergenerationeller Teilhabe an der Digitalisierung

Ältere Generationen fühlen sich heute nicht mehr so vom technischen Fortschritt ausgeschlossen wie noch vor zehn Jahren. Die Bedinung von Geräten hat sich durch intensive Forschung zu Usability und UI-Design stark verbessert, sodass weniger Vorwissen erforderlich ist. Es existieren Normen zur Gestaltung von Softwareoberflächen, die breite Anwendung finden. Für Entwickler:innen gehört dieser Bereich als Schwerpunkt zur Ausbildung oder zum Studium. Für komplexere Technik, sowohl analog als auch digital, stehen **Assistenz-Apps zur Verfügung, die bei Fragen geduldig zur Seite stehen**. Dadurch entfällt das oft beschämende Nachfragen bei jüngeren Generationen, und die häufig beobachtete Abneigung für Technik bei älteren Menschen baut sich nach und nach ab.


### Freie Software

Wenn man jetzt zehn Jahre zurückblickt, haben sich viele Sachen in der Gesellschaft zum positiven verändert und **an vielen dieser Verbesserungen hat gute Software einen wesentlichen Anteil**. Interessanterweise war dafür auch irgendwie die Pandemie verantwortlich: Die Corona-Warn-App war die erste große öffentliche IT-Investition, die quasi von Anfang an als Freie Software entwickelt wurde. Kurz danach hat sich in immer mehr Kommunen und Ländern das Prinzip ***Public Money Public Code*** durchgesetzt und es hat sich ein ziemlich **direkter Kommunkationskanal zwischen Nutzer- und Entwickler:innen** etabliert. Inzwischen gehört es zum Standard, dass das Melden eines Fehlers oder eines Verbesserungsvorschlages sehr niedrigschwellig funktioniert. Früher haben die Leute ihre Frustration oft einfach in eine 1-von-5-Stern-Bewertung kanalisiert, die niemandem geholfen hat. Inzwischen aber ist **konstruktives Feedback weit verbreitet**, weil es unkompliziert ist und sich oft innerhalb kurzer Zeit schon was verbessert.

Seit der Staat öffentlich und nachvollziehbar Geld für die Entwicklung und Pflege Freier Software ausgibt, machen das übrigens auch immer mehr Firmen und Privatleute. Entweder spenden Sie an einzelne Projekte oder über eine Stiftung, die sich gezielt und transparent um die Verteilung der Mittel an wichtige Projekte kümmert. Im Schnitt **geben die Bundesbürger:innen pro Kopf und Tag inzwischen 10 Cent für freie Software aus**.

Neben der Software und ihrer Benutzbarkeit hat sich auch die Dokumentation verbessert. Es gibt zwar immer noch Leute, die gerne sogenannte Manpages (elektroische Handbücher) lesen, aber populärer sind **interaktive Assistenten, mit denen man ziemlich schnell rausfindet, ob und wie ein Problem lösbar ist**.

Proprietäre Software gibt es auch noch, aber wenn man als Hersteller ernst genommen werden will, geht das nur mit einem **transparenten und nachvollziehbaren Geschäftsmodell**. Die Zeiten in denen kostenlose Apps einfach heimlich Daten gesammelt und weiterverkauft haben sind zum Glück vorbei.

Das gilt zum Glück auch für die großen IT-Sicherheitsskandale. Der Erpressungsangriff auf eine Uniklinik im September 2020 brachte offenbar das Fass zum überlaufen. Seit Ende 2021 gibt es im BSI eine **kompetent besetzte "Pentest"-Abteilung**, welche systematisch die kritische öffentliche IT-Infrastruktur auf Schwachstellen absucht und mit robustem Nachdruck auf deren Abstellung hinwirkt. Da inzwischen in wichtigen Bereichen fast ausschließlich auf weit verbreitete und gut gewartete Freie Software gesetzt wird, gibt es aber auch einfach nicht mehr so viel zu tun.


---

## Schlussbemerkung

Inspiriert durch die [11 Forderungen der Bits- und Bäume-Bewegung](https://bits-und-baeume.org/forderungen/info/de) wird in diesem Text eine Utopie beschrieben, wie Deutschland 2030 aussehen könnte, wenn die Digitalisierung sich primär am Nachhaltigkeitsprinzip und den Bedürfnissen der Menschen ausrichtet.

Der Text ist vermutlich zu optimistisch und in einigen Aspekten kontrovers. Er soll hauptsächlich zum Nachdenken und zur Diskussion anregen. Feedback gerne an `dresden{ät}bits-undbaeume.org`.

