---
layout: post
title: System Change mit Instagram?? – Social Media und die Zivilgesellschaft
date: 2021-12-22 10:00
author: cark
summary: Über die Abhängigkeit der Zivilgesellschaft von Social-Media-Monopolen und was sich dagegen tun lässt
image: /assets/images/2rC3-thumbnails1.jpg
---


[**Online-Podiumsdiskussion** auf dem rC3-2021 <time datetime="2021-12-28 11:30:00 +0100">28.12.2021 11:30 Uhr</time>]( https://dresden.bits-und-baeume.org/system-change).


**Video-Links:**

- [https://media.ccc.de/...](https://media.ccc.de/v/rc3-2021-chaosstudiohamburg-676-system-change-mit-instagram-social-media-und-die-zivilgesellschaft)
- https://digitalcourage.video/... (kommt noch)
- [https://www.youtube.com/watch?v=mugbVJ7dI_c](https://www.youtube.com/watch?v=mugbVJ7dI_c)

**Veranstaltung im Pretalx:** <https://pretalx.c3voc.de/rc3-2021-chaosstudiohamburg/talk/review/MXUSE9RQJC8T9FEVFUVAL8K3RYHVEE8H>

**Link zum [Fahrplan](https://rc3.world/2021/public_fahrplan)-Eintrag:** <https://rc3.world/2021/public_fahrplan#e34e5c7f-fdcb-5d8a-8273-16b1654ca5b8>



Egal ob global oder lokal – die Gesellschaft steht vor immensen Herausforderungen und der Politik alleine traut kaum jemand zu, diese auch nur halbwegs vernünftig zu meistern. Um so mehr braucht es eine kritische und aktiv organisierte Zivilgesellschaft. Dummerweise sind diese Organisationen aber sehr stark auf ihre Reichweite in den sog. "Sozialen Medien" angewiesen. Diese kritische Infrastruktur unterliegt der totalen Kontrolle von Unternehmen wie Meta (Instagram, Whatsapp), Alphabet (Youtube, Gmail), deren Geschäftsmodell (Überwachung und Beeinflussung von Milliarden von Menschen) ebenso erfolgreich wie problematisch ist.

In der Podiumsdiskussion wird die Situation aus verschiedenen Perspektiven betrachtet (FridaysForFuture, EndeGelände, Digitalcourace, Konzeptwerk neue Ökonomie, PIRATEN, Extinction Rebellion) und nach Lösungsmöglichkeiten für das Dilemma gesucht.


## Podiumsgäste (anklicken für mehr Infos):


<!-- ------ -->
<details>
<summary>Pauline Brünger (Fridays for Future Sprecherin)</summary>
<ul>
<li><a href="https://fridaysforfuture.de">https://fridaysforfuture.de</a></li>
<li>Twitter: <a href="https://twitter.com/PaulineBruenger">@PaulineBruenger</a></li>
<li>Instagram: <a href="https://www.instagram.com/paulinebruenger">@paulinebruenger</a></li>
<li>Mastodon (FFF): <a href="https://chaos.social/@fff">@fff@chaos.social</a></li>
<li>Facebook (FFF): <a href="https://www.facebook.com/fridaysforfuture.de/">@fridaysforfuture.de</a></li>
</ul>


</details>

<!-- ------ -->
<details>
<summary>padeluun (Digitalcourage Gründer)</summary>
<ul>
<li><a href="https://padeluun.de">https://padeluun.de</a></li>
<li><a href="https://digitalcourage.de">https://digitalcourage.de</a></li>
<li>Mastodon: <a href="https://digitalcourage.social/@padeluun">@padeluun</a></li>
<li>Twitter: <a href="https://twitter.com/padeluun">@padeluun</a></li>
</ul>


</details>

<!-- ------ -->
<details>
<summary>Struppi (EndeGelände Social Media Team)</summary>
<ul>
<li><a href="https://www.ende-gelaende.org/">https://www.ende-gelaende.org/</a></li>
<li>Mastodon: <a href="https://climatejustice.global/@ende_gelaende">@ende_gelaende@climatejustice.global</a></li>
<li>Peertube: <a href="https://climatejustice.video/c/ende_gelaende/">@ende_gelaende</a></li>
<li>Twitter: <a href="https://twitter.com/Ende__Gelaende">@Ende__Gelaende</a></li>
<li>Instagram: <a href="https://www.instagram.com/ende__gelaende/">@ende__gelaende</a></li>
<li>Mehr Kanäle: <a href="https://www.ende-gelaende.org/ueber-uns/">https://www.ende-gelaende.org/ueber-uns/</a></li>
</ul>


</details>

<!-- ------ -->
<details>
<summary>Max Bömelburg (Konzeptwerk Neue Ökonomie)</summary>
<ul>
<li><a href="https://konzeptwerk-neue-oekonomie.org/">https://konzeptwerk-neue-oekonomie.org/</a></li>
<li>Twitter: <a href="https://twitter.com/NeueOekonomie">@NeueOekonomie</a></li>
<li>Instagram: <a href="https://www.instagram.com/konzeptwerk_neue_oekonomie/">@konzeptwerk_neue_oekonomie</a></li>
<li>Facebook: <a href="https://www.facebook.com/Konzeptwerk/">@Konzeptwerk</a></li>
<li>In der Diskussion (und extended Q&amp;A) erwähnte Links:<ul>
<li><a href="https://contacts.knoe.org/civicrm/contribute/transact?reset=1&amp;id=19">Spendenseite</a></li>
<li><a href="https://www.endlich-wachstum.de/">Endlich Wachstum</a> Bildungsmaterialien für eine sozial-ökologische Transformation (auch zu Digitalisierungsthemen)</li>
<li><a href="https://digital-bewegt.org/">digital bewegt</a> - Potentiale digitaler Technik für eine sozial-ökologische Transformation</li>
<li><a href="https://blog.oeko.de/digitaler-co2-Fussabdruck">Der CO2-Fußabdruck unseres digitalen Lebensstils (Ökoinstitut)</a></li>
</ul>
</li>
</ul>


</details>

<!-- ------ -->
<details>
<summary>Stephanie Henkel/@Ückück (2. Vorsitzende Piraten Sachsen)</summary>
<ul>
<li>Mastodon:<ul>
<li><a href="https://dresden.network/@ueckueck">@ueckueck@dresden.network</a></li>
<li><a href="https://dresden.network/@piratensachsen">@piratensachsen@dresden.network</a></li>
</ul>
</li>
<li>Twitter:<ul>
<li><a href="https://twitter.com/@S_HenkelDD">@S_HenkelDD</a></li>
<li><a href="https://twitter.com/@Piraten_SN">@Piraten_SN</a></li>
</ul>
</li>
<li>Facebook: <a href="https://facebook.com/PiratenSachsen">@PiratenSachsen</a></li>
<li>Instagram: <a href="https://www.instagram.com/piraten.sachsen">@piraten.sachsen</a></li>
</ul>


</details>

<!-- ------ -->
<details>
<summary>Thomas Pfeiffer (Extinction Rebellion Social Media Team)</summary>
<ul>
<li><a href="https://extinctionrebellion.de">https://extinctionrebellion.de</a></li>
<li>Mastodon: <a href="http://social.rebellion.global/@xrgermany">@xrgermany@social.rebellion.global</a></li>
<li>Peertube: <a href="https://tube.rebellion.global/accounts/germany/video-channels">@xrgermany@social.rebellion.global</a></li>
<li>Twitter: <a href="https://twitter.com/ExtinctionR_DE">@ExtinctionR_DE</a></li>
<li>Instgram: <a href="https://www.instagram.com/extinctionrebelliondeutschland/">@extinctionrebelliondeutschland</a></li>
<li>Facebook: <a href="https://www.facebook.com/ExtinctionRebellionDeutschland/">@ExtinctionRebellionDeutschland</a></li>
<li>Youtube: <a href="https://www.youtube.com/channel/UCzRo82VzXa_XgySyH-D5lqA/f">@ExtinctionRebellionDeutschland</a></li>
</ul>


</details>

<!-- ------ -->
<details>
<summary>Moderation: Carsten Knoll (Bits&Bäume Dresden)</summary>
<ul>
<li><a href="https://dresden.bits-und-baeume.org/">https://dresden.bits-und-baeume.org/</a></li>
<li>Mastodon: <a href="https://social.tchncs.de/@cark">https://social.tchncs.de/@cark</a></li>
<li>Codeberg: <a href="https://codeberg.org/cknoll">https://codeberg.org/cknoll</a></li>
<li>Github: <a href="https://github.com/cknoll">https://github.com/cknoll</a></li>
<li>Projekte (Auswahl):<ul>
<li><a href="https://sober-arguments.net">https://sober-arguments.net</a> (Web-App-Prototyp für konstruktivere Diskussionen)</li>
<li><a href="https://moodpoll.uber.space">https://moodpoll.uber.space</a> (Web-App u.a. zum systemischen Konsensieren)</li>
<li><a href="https://codeberg.org/open/fedipolitik">https://codeberg.org/open/fedipolitik</a> Übersicht politischer Entitäten im Fediverse</li>
<li><a href="https://plq.de">https://plq.de</a> (Gründungsinitiative der Partei für Lebensqualität)</li>
</ul>
</li>
</ul>


</details>



Wen das Thema grundsätzlich interessiert, kann sich schon vorab gerne mit Fragen und Anregungen unter

`system-change{ät}dresden.bits-und-baeume.org`

melden. Und eventuell gibt es auch nach der Diskussion noch Bedarf für weiteren Austausch...


## Material zur inhaltlichen Einstimmung:

- <https://fahrplan.bits-und-baeume.org/events/77.html> Ideen-gebende Veranstaltung: *"Kampagnen-Brainstorming: Facebook zerschlagen?"* auf der Bits&Bäume-Konferen 2018
- <https://enteignetfacebook.global/> ZDF Magazin Royale zu Facebook: Sehr sehenswerte Sendung (+ Song)
- <https://yewtu.be/watch?v=lFkpB8X-Rm8&t=4259> Sehr gutes `#JungUndNaiv`-Interview mit Sarah-Lee Heinrich (GJ-Sprecherin). Time Stamp auf Schlüsselstelle zur Diskussionskultur in den sozialen Medien.
- <https://yewtu.be/watch?v=HGyRk0lW0FE> Viktor Schlüter: Warum Klimaschutz und Datenschutz zusammengehören

