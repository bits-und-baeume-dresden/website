---
layout: post
title: B&B Dresden OB Wahlprüfsteine
date: 2022-04-28 19:00
author: "B&B-Team"
summary: "B&B Dresden Wahlprüfsteine zur Oberbürgermeister:innenwahl in Dresden 2022"
image: /assets/images/2022-04-stones.jpg
---


Die folgenden Wahlprüfsteine orientieren sich inhaltlich an den [Forderungen der Bits&Bäume Konferenz 2018](https://bits-und-baeume.org/forderungen/info/de). Wir als Bits&Bäume-Dresden versenden diese an Kandierende bei der anstehenden Oberbürgermeister:innenwahl. Die Antworten sind inzwischen eingegangen und hier in der Reihenfolge des Eintreffens veröffentlicht. Zum Weiterleiten empfehlen wir den folgenden Link: <https://dresden.bits-und-baeume.org/wahlpruefsteine>.

---

Unsere Eindrücke zu den Kandidierenden haben wir [hier in diesem Video](https://tube.avensio.de/w/bfaJh8QeZhDNFkVFBqdD9z) zusammengefasst.

In [diesem Thread auf unserem Mastodon-Account](https://dresden.network/@BitsUndBaeumeDresden/108418734884229955) veröffentlichen wir bis zur Wahl kurze Übersichten zu den einzelnen Fragen.

---

## Bits&Bäume-Wahlprüfsteine zu Digitalisierung und Nachhaltigkeit, OB Wahl Dresden 2022

### 1. Welche Ideen haben Sie, die Dresdner Bürger:innen mehr in demokratische Prozesse einzubeziehen?

#### Eva Jähnigen (B90/Die Grünen)

Dresden hat durch die Stadtbezirksverfassung und die
Bürgerbeteiligungssatzung gute Ansätze, denn so können Bürgerinnen und
Bürger die Durchführung von Beteiligungsverfahren zu städtischen
Vorhaben selbst auf einfache Weise beantragen. Umgesetzt wurde diese
Satzung jedoch bisher nicht. Das will ich dringend angehend und die
Ziele der Satzung mit genügend Personal, einem geeigneten IT-System und
einer Koordination in alle Verwaltungsbereiche von der
Oberbürgermeisterin untersetzen. Außerdem will ich als
Oberbürgermeisterin ein Bürgerhaushaltsverfahren einführen und den
Haushalt der Stadt hierfür transparenter und allgemeinverständlicher
machen. Teil der Bürgerbeteiligung muss die Beteiligung von Kindern und
Jugendlichen sein und eine Stärkung der Arbeitsmöglichkeiten der
Stadtbezirksbeiräte. Zur besseren Verständlichkeit will ich Dokumente
künftig barrierefrei gestalten und mehr leichte Sprache einsetzen.


#### Jan Pöhnisch (PARTEI)

Ich lebe seit nun mehr als drei Jahren in dieser Stadt und konnte feststellen, dass sich der gemeine Dresdner in seinem Intellekt kaum von einem Bauern aus tiefster Provinz unterscheidet. Die Teilhabe an demokratischen Prozessen überstiege all seine Fähigkeiten in einem Maß, dass er Kopfschmerzen bekommen und dadurch wütend werden würde. Ich werde den Menschen hier diese Bürde abnehmen und sie väterlich in einen besseren Morgen führen.


#### Martin Schulte-Wissermann (Piraten)

Bürger:innenentscheide sind ein guter Weg, den Anwohnenden eine Möglichkeit zu bieten, ihre Stadt mitzugestalten. Am Beispiel des Sachsenbads haben wir gesehen, wie gut solche Angebote wahrgenommen werden. Der bisherige OB hat solche Initiativen allerdings bis jetzt ignoriert bzw. gar torpediert.
Prinzipiell bin ich ein großer Freund von Sprechstunden und klarer, transparenter Kommunikation. So biete ich alle 14 Tage eine Sprechstunde zur Oberbürgermeister:innenwahl an, die gerade die dauerhafte Sprechstunde der Neustadtpiraten ersetzt.
Auch muss es eine einfache Berichterstattung über die stadtpolitischen Ereignisse geben. Wir versuchen dies als Piraten durch Podcasts, Videos und andere Berichte nach Stadtratssitzungen zu realisieren und unsere Neustadtpiraten veröffentlichen nach jeder Stadtbezirksbeiratssitzung einen Bericht. Als Oberbürgermeister gäbe es aber noch viel mehr Möglichkeiten, durch multimediale Informationsverbreitung mehr Transparenz zu schaffen.

#### André Schollbach (Linke)

Über wichtige Themen sollen die Dresdnerinnen und Dresdner selbst entscheiden. Deshalb schlage ich
vor, zu jeder Wahl (Bundestagswahl, Landtagswahl, Europawahl, Kommunalwahl) auch
Bürgerentscheide durchzuführen. Zudem kann das beteiligungsorientierte Instrument des
Bürgerhaushalts dazu beitragen, die Bürgerinnen und Bürger aktiv in die städtische Finanzpolitik
einzubeziehen. Darüber hinaus haben CDU und SPD im Sächsischen Landtag durch eine Änderung
der Sächsischen Gemeindeordnung die in Dresden zunächst beabsichtigte Gleichstellung der früheren
Ortsbeiräte und der Ortschaftsräte verhindert. Ich werde mich gegenüber dem Sächsischen Landtag
dafür einsetzen, dass die Gemeindeordnung in diesem Punkt verbessert wird, um die angestrebte
Gleichstellung zu erreichen.


#### Albrecht Pallas (SPD)

Was ich nicht will, ist eine Pseudo-Beteiligung in Prozessen. Die
Bürgerbeteiligungssatzung muss so schnell wie möglich umgesetzt werden.
Klar ist: Nur das Zusammenwirken einer engagierten Verwaltung mit
engagierten Bürger:innen machen eine Stadt wirklich lebenswert. Eine
demokratische Stadt muss für Beteiligungsmöglichkeiten an politischen
Prozessen sorgen und Verfahren für Bürger:innen verständlich und
transparent gestalten. Ich habe daher im Landtag dafür gekämpft, dass
die Stadtbezirke über die direkt gewählten Stadtbezirksbeiräte mit
eigenen Geldern ausgestattet wurden, mit denen sie unbürokratisch
Projekte und Initiativen fördern können, die sich für eine aktive und
soziale Nachbarschaft stark machen. Dazu werde ich die bestehenden
Stadtteilfonds zu Bürgerbudgets weiterentwickeln, mit denen Bürger:innen
in einem basisdemokratischen Prozess direkt für die Finanzierung ihrer
Ideen werben können. Wir haben hiermit in Pieschen und der Johannstadt
bereits gute Erfahrungen gesammelt.


#### Dirk Hilbert (FDP)

Die Dresdner erwarten eine bürgerfreundliche, lösungsorientiert und zügig arbeitende Verwaltung, die sich sachbezogen auf praktikable Lösungen konzentriert. Das betrifft Ortschaften und Stadtbezirke ebenso wie die Stadt als Ganzes. Dresden verfügt über eine sehr gute Bürgerbeteiligungssatzung. Diese bedarf jedoch noch einer Optimierung zwischen den Forderungen nach maximaler Transparenz und Mitwirkung einerseits und Verfahrensbeschleunigung andererseits. In meinem Wahlprogramm sind die Details beschrieben.


 ---

### 2. Gibt es in Ihren Augen bereits jetzt digitale Hilfsmittel zur demokratischen Teilhabe in Dresden? Was würden Sie an der aktuellen Lage gern ändern oder ausbauen?

#### Eva Jähnigen (B90/Die Grünen)

Bisher gibt es digitale Hilfsmittel in unserer Stadtverwaltung zur
Beteiligung nur in einzelnen, auf bestimmte Aufgaben oder die Tätigkeit
einzelner Ämter bezogenen Fällen. Ich will die Digitalisierung so
vorantreiben, dass einheitliche Formate entstehen, die auch
querschnittsbezogen, genutzt werden können. Diese müssen barrierefrei
nutzbar werden. Zudem will ich die bisherigen Ansätze zur digitalen
Transparenz der Stadtverwaltung (Informationsfreiheitssatzung)
ausweiten.


#### Jan Pöhnisch (PARTEI)

Es gibt derzeit die Möglichkeit Stadtratssitzungen per Stream zu verfolgen. Hier werde ich noch eine Chatfunktion und die Möglichkeit von Zuschauer-Voting per App einführen. Das Stadtratsmitglied mit den wenigsten Stimmen muss eine Ekelprüfung bestehen oder bekommt nichts mehr zu essen. Des Weiteren gibt es für jedermann die Möglichkeit online Petitionen einzureichen, die dann entweder schon im Ausschuss oder später im Stadtrat zu nichts führen. An der Stelle könnte man anknüpfen. Die Bürger sollten noch viel mehr online einreichen können, ohne das etwas passiert.

#### Martin Schulte-Wissermann (Piraten)

Einige Angebote der Stadt gibt es bereits, auch wenn ich mir noch mehr wünschen würde.
So sind ein nützliches Mittel für Transparenz die bisherigen Videoübertragungen und Aufnahmen der Stadtratssitzungen. Auch die städtischen ePetitionen sind hilfreich und die gut gepflegte Website und die verschiedenen Themenstadpläne bieten viele Informationen.
Ich wünsche mir jedoch, dass die Kontaktaufnahme zur Stadt einfacher wird. Zwar ist diese sehr um ihre Social Media-Präsenz bemüht, allerdings ist sie nur auf zentralisierten, aus Datenschutzsicht bedenklichen, Plattformen zu erreichen. Als Alternative stelle ich mir eine Präsenz in den freien dezentralen Sozialen Medien vor, aber dazu mehr bei Frage 3.
Ein besonderes Anliegen ist es mir, die Kommunikation mit Behörden einfach und sicher zu machen und z.B. endlich die Möglichkeit kostenfreier verschlüsselter Mail-Kommunikation einzuführen. Da es für die Umsetzung viele Wege gibt, muss dafür mit der Gesellschaft in einen Dialog getreten werden.


#### André Schollbach (Linke)

Als vorhandenes digitales Hilfsmittel zur demokratischen Teilhabe ist die e-Petition zu nennen
(https://www.dresden.de/de/leben/gesellschaft/buergebeteiligung/epetition.php). Hiervon wird rege Gebrauch
gemacht, was ich sehr erfreulich finde. Weiterhin werden Sitzungen des Stadtrates regelmäßig im
Livestream übertragen, damit interessierte Dresdnerinnen und Dresdner die Debatten unkompliziert
verfolgen können. Instrumente zur demokratischen Teilhabe, deren Einsatz auch digital möglich ist,
sollten entsprechend ausgebaut werden.


#### Albrecht Pallas (SPD)

Wir müssen als Verwaltung schneller und einfacher werden angesichts der
großen Herausforderungen bei Infrastrukturvorhaben etwa beim
Klimaschutz. Alle Dienstleistungen müssen wir auch digital nutzen
können. Jeder Haushalt und jede Schule in Dresden muss einen
gigabitfähigen Internetanschluss haben. Dresdner Schülerinnen und
Schüler müssen die Möglichkeit haben, digitale Kompetenzen zu erwerben.
Dazu gehören ausreichend viele, für alle kostenfrei zugängliche WLAN
Access-Points. Ich will, dass Digitalisierung ein Vorzeigeprojekt
Dresdens wird. Die Menschen sollen durch Digitalisierung Zeit für andere
Dinge des Lebens gewinnen. Schnellere Verfahren, mehr Dienstleistungen,
weniger Aufwand, mehr digitale Partizipation sowie Informations- und
Kommunikationstechnologien sollen v.a. dazu dienen, ökologische und
soziale Verbesserungen zu erreichen. Das Online-Angebot der Stadt unter
dresden.de ist ein guter Startpunkt, muss aber insgesamt übersichtlicher
und noch barrierefreier werden.


#### Dirk Hilbert (FDP)

Für mich ist die Vereinfachung und Beschleunigung von Verwaltungsabläufe ein Kernthema und die Digitalisierung der Schlüssel dazu. Petitionen sind heute bereits elektronisch möglich: [*E-Petition \| Landeshauptstadt Dresden*](https://www.dresden.de/de/leben/gesellschaft/buergebeteiligung/epetition.php), Bürgerversammlungen können virtuell stattfinden. Ich schätze aber auch das persönliche Gespräch, was von vielen Bürgern in den letzten Jahren genutzt worden ist. Seit 2016 bis heute habe ich über 35 Bürgersprechstunden mit mehr als 300 Einzelgesprächen durchgeführt.

 ---

### 3. Haben Sie vor, die digitale Teilhabe für Bürger:innen zu erleichtern, die nicht Dienste von Monopol-Plattformen wie Twitter und Facebook nutzen wollen?


#### Eva Jähnigen (B90/Die Grünen)

Um die digitale Teilhabe der Bürger:innen zu erleichtern werde ich
solche Bereiche identifizieren, in denen Teilhabe an Informationen
prioritär oder gar ausschließlich über Monopol-Plattformen wie Facebook
organisiert sind. Informationen und Möglichkeiten zur Teilhabe müssen
primär auf neutralen oder hoheitlichen und datenschutzrechtlich
unbedenklichen Plattformen angeboten werden. Ich werde dafür Sorge
tragen, dass die Dresdner Stadtverwaltung als Vorzeigebeispiel vorangeht
und neben der eigenen Webseite auch neue, aber unbedenkliche
Plattformen, wie die vom Bundesbeauftragten für Datenschutz und die
Informationsfreiheit empfohlene Twitter-Alternative Mastodon zur
Bereitstellung von Informationen und Teilhabemöglichkeiten nutzt.


#### Jan Pöhnisch (PARTEI)

Nein. Wenn sich Menschen weigern, ihre Daten an Facebook weiterzugeben, ist davon auszugehen, dass die irgendetwas im Schilde führen. Ich bin solchen Leuten gegenüber sehr misstrauisch.


#### Martin Schulte-Wissermann (Piraten)

Unbedingt. Es ist zwar sehr löblich, dass die Stadt gut auf einigen Social Media-Plattformen vertreten ist und antwortet - nur leider ist sie es nur auf den "Walled Gardens". Um an schnelle Informationen und Antworten zu gelangen, sollte die Stadt jedoch auch auf Plattformen vertreten sein, die nicht großen Tech-Firmen oder exzentrischen Milliardären gehören. Hier bieten sich z.B. solche im Fediverse an.
Denkbar wären Veranstaltungskalender über Mobilizon, die Stadtratsvideos auf PeerTube oder Behördennews über einen Microbloggingdienst wie Mastodon. Auch eigene Serer, sogenannte Instanzen, der Stadt wären denkbar. Als Beispiel könnte sich die Stadt die bereits bestehenden Mastodon-Server des Bundes nehmen.
Ich selbst bin ebenfalls im Fediverse unter @mswdresden@dresden.network und die Piraten Dresden unter @piratendresden@pirati.ca zu erreichen.


#### André Schollbach (Linke)

Monopole sind in verschiedensten Bereichen mit nicht unerheblichen Nachteilen verbunden –
gesellschaftlich, wirtschaftlich und finanziell. Daher halte ich es für sinnvoll, auch bei
verwaltungsrelevanter Software darauf hinzuwirken, dass Monopolstellungen nicht entstehen oder
zurückgedrängt werden.


#### Albrecht Pallas (SPD)


Mit der "Dresdner Debatte" gibt es bereits eine gute und erfolgreiche
Grundlage der digitalen Partizipation für Dresdnerinnen und Dresdner.
Die Kerndienstleistungen der Stadtverwaltung sollten allein aus
Datenschutzgründen generell nicht über die genannten Plattformen
angeboten oder abgewickelt werden. Auf der Ebene der EU und Bund und
Ländern gibt es bereits verschiedene Ideen und Projekte, um die
Abhängigkeit von den großen Plattformen zu verringern. Ich werde daher
nach der Wahl prüfen, ob wir uns als Stadt Dresden für entsprechende
Pilotprojekte bewerben und damit einen wichtigen Beitrag zu einer
Weiterentwicklung der sicheren, stabilen und zugänglichen digitalen
Angebote für die Dresdnerinnen und Dresdner können.


#### Dirk Hilbert (FDP)

Aktuell nicht, stehe aber über das BOB und das Presseamt für Anregungen und Hinweise gern zur Verfügung. Ob Twitter und Facebook eine digitale Teilhabe, im wahrsten Sinne des Wortes, ist, bin ich mir nicht sicher.

---

### 4. Halten Sie das Quasi-Monopol von Microsoft bei der Bereitstellung von verwaltungsrelevanter Software (Betriebssystem, Office) für problematisch und was planen Sie ggf. in Ihrer Amtszeit zu tun, um Abhängigkeiten zu reduzieren (Stichwort "Digitale Souveränität")?


#### Eva Jähnigen (B90/Die Grünen)

Ich sehe diese Problematik und strebe an, dass die digitale
Unabhängigkeit der Landeshauptstadt Dresden von einzelnen
Softwareprodukten und Herstellern gestärkt wird: Die Anschaffung von
Software, Nutzungslizenzen und Beratungsdienstleistungen im IT-Bereich
wird künftig unter dem Aspekt betrachtet und ggf. neu ausgerichtet, ob
die anzuschaffende Software einen Um- oder Ausstieg ermöglicht oder
unnötig erschwert (Vendor-Lockin-Effekt). Fachverfahren der Stadt sollen
in Zukunft grundsätzlich als plattformneutrale Dienste erstellt werden.

#### Jan Pöhnisch (PARTEI)

Ich persönlich nutze Openoffice und finde es kacke. Ich kandidiere für das Amt des Oberbürgermeisters, weil ich einen Dienstcomputer mit einer ganzen Palette der sehr guten Microsoft-Produkte haben will. Microsoft - Be what's next.


#### Martin Schulte-Wissermann (Piraten)

Dieses Monopol ist sehr problematisch und ich setze mich für Freie Software in der Verwaltung ein. Wann wenn nicht jetzt wäre es ein guter Zeitpunkt den Weg zu mehr Freier Software in der Verwaltung zu beschreiten hinsichtlich der aktuellen Entwicklungen auf Bundes- und Länderebene bzgl. eines einheitlichen Behörden-Desktops (1). Freie Software minimiert Abhängigkeiten von einzelnen Anbietern und fördert damit den Wettbewerb, welcher oft regionale Unternehmen profitieren lässt. Durch die Verwendung von offenen und freien Formaten ist für die Verwaltung ein barrierefreier Zugang zu den Daten jederzeit möglich und die Kommunikation mit den Bürger:innen wird erleichtert.

(1) "einheitlicher Behördendesktop": https://www.golem.de/news/verwaltung-budget-fuer-einheitlichen-behoerden-desktop-unklar-2204-164673.html


#### André Schollbach (Linke)

Monopole sind in verschiedensten Bereichen mit nicht unerheblichen Nachteilen verbunden –
gesellschaftlich, wirtschaftlich und finanziell. Daher halte ich es für sinnvoll, auch bei
verwaltungsrelevanter Software darauf hinzuwirken, dass Monopolstellungen nicht entstehen oder
zurückgedrängt werden.



#### Albrecht Pallas (SPD)

Der Einsatz von Open Source-Software muss gerade im Hinblick auf
Digitale Souveränität in der öffentlichen Verwaltung stärker Einzug
halten. Dadurch können wir die Abhängigkeit von den Hyperscalern
reduzieren und die Effizienz und Effektivität in Entwicklung und
Inbetriebnahme steigern. Open Source trägt auch dazu bei, den
Datenschutz umfassender oder noch transparenter zu gestalten.

Selbstverständlich müssen hierbei die von der öffentlichen Verwaltung
genutzten digitalen Plattformen den Anforderungen und gesetzlichen
Bestimmungen zur Erfüllung der staatlichen Aufgaben gerecht werden.
Dabei muss zuallererst gesichert werden, dass die Systeme sicher, stabil
und möglichst barrierefrei organisiert sind. Monopole halte ich nicht
nur bei den digitalen Angeboten für problematisch.

Ich werde mich daher nach der Wahl zusammen mit der Verwaltung, dem
Stadtrat und der Stadtgesellschaft über sinnvolle und effiziente
Lösungen beraten und in der Stadtverwaltung Pilotprojekte initiieren.



#### Dirk Hilbert (FDP)

Die Globalisierung und Abhängigkeiten von Quasi-Monopolen sind sichtbar. Aktuell überwiegen die Vorteile die Nachteile. Daher gibt es keine konkreten Änderungspläne.


---

### 5. Welche Möglichkeiten für die Stadt Dresden sehen Sie, sich für eine bessere Bildung im Bereich Digitalisierung und Nachhaltigkeit einzusetzen?


#### Eva Jähnigen (B90/Die Grünen)

Gute Bildung ermöglicht Chancengerechtigkeit und bietet nicht zuletzt
Auswege aus dem Fachkräftemangel. Die Qualität von Schul- und
Kita-Bauten als Aufenthaltsorte für Heranwachsende muss erhöht werden.
Ziel ist die gleichwertige Attraktivität aller Lernumgebungen für alle
Schulformen. Dazu gehören Begrünung und beteiligungsorientierte
Gestaltung der Außenflächen und Sportstätten sowie in der Schließzeit
der Schulen die unbürokratische, öffentliche Nutzungsmöglichkeit. Denn
Schulen und Kitas sind wertvoller öffentlicher Raum. Die digitalisierte
Schule muss Schüler\*innen mit Selbstverständlichkeit zur Verfügung
stellen, was ihnen im Alltag ständig begegnet. Die Hardware kommt über
den Digitalpakt in die Schulen und als Ziel müssen wir eine
Vollabdeckung mit Endgeräten und der endsprechenden Infrastruktur
verfolgen. Mit der Stadtbibliothek sind Modelle zu entwickeln, die
Lizenzen zentral anschaffen.

#### Jan Pöhnisch (PARTEI)

Fridays for Future verbieten und jeden Schwänzer von der Bereitschaftspolizei in die Schule zurückprügeln lassen.


#### Martin Schulte-Wissermann (Piraten)

Ich setze mich für lebenslange freie Bildung ein. Z.B. möchte ich Bibliotheken dabei unterstützen, Informationsveranstaltungen anzubieten und mit neuen Projekten wie der Saatgutbibliothek Leute für Bildung zum Erleben zu begeistern.
Außerdem kann ich mir themenbezogene Projektförderung in diesen Bereichen vorstellen.


#### André Schollbach (Linke)

Bildung im Bereich Digitalisierung und Nachhaltigkeit einzusetzen?
Das beginnt bereits bei der Ausstattung unserer Schulen mit moderner Ausstattung, damit die
Schülerinnen und Schüler von vornherein entsprechende Kompetenzen erwerben können. Die Stadt
Dresden kann zudem Bildungsangebote in diesen Bereichen stärken, in dem sie diese zielgerichtet
fördert.

#### Albrecht Pallas (SPD)

Die digitale Gesellschaft in Dresden will ich mit und für die
Bürgerinnen und Bürger gestalten. Digitalisierung darf kein Selbstzweck
sein. Die Möglichkeiten der Digitalisierung sollen das Leben der
Menschen erleichtern und sich an ihren Bedürfnissen orientieren. **Es
muss dabei sichergestellt sein, dass in einer digitalen Stadt alle
mithalten können.** Insbesondere möchte ich die bestehenden
Bildungseinrichtungen in unserer Stadt, die Schulen, Stadtbibliotheken,
universitären Einrichtungen, Museen und die Volkshochschule dabei
unterstützen, in **allen Stadtteilen** niedrigschwellige
Bildungsangebote für die verschiedenen Zielgruppen in den Bereichen
Digitalisierung und Nachhaltigkeit zu entwickeln.

Ein Schwerpunkt wird die Fortbildung für städtische Bedienstete sein.
Gemeinsam mit dem Fortbildungszentrum der Hochschule Meißen (FH) und
weiteren Bildungsträgern will ich jedem/r Beschäftigten ein Angebot
machen, sich für eine digitale Verwaltung vorzubereiten.



#### Dirk Hilbert (FDP)

Schulische Bildungsinhalte werden durch das Land Sachsen bestimmt. Wir haben in Dresden jedoch eine Reihe von Lernlaboren und Rechenzentren, die mit Leben gefüllt sind. Auch für unsere reiferen Bürger und Bürgerinnen gibt es Möglichkeiten, sich im digitalen Bereich fortzubilden. Wir sollten auch beachten, dass nicht alles von Staatswegen organisiert werden muss und jeder eine gewisse Eigenverantwortung hat.

---

### 6. Welche Möglichkeiten sehen Sie, Bildungseinrichtungen bei Aufbau und Wartung digitaler Infrastruktur zu unterstützen, z.B. mit IT-Personal analog zu Hausmeister:innen?


#### Eva Jähnigen (B90/Die Grünen)


Ich bin der Auffassung, dass die Stadt als Trägerin der Kitas und
Schulen, die Verantwortung für die IT übernehmen muss und nicht die
Lehrkräfte Für die Kitas und Schulen müssen daher genügend
Mitarbeiter\*innen zur Verfügung stehen, die diese Technik warten und
betreiben können -- und die Ausgaben für dieses Personal müssen in
angemessenem Umfang zu den Investitionen liegen.

#### Jan Pöhnisch (PARTEI)

Wer als Laie bereits das Vergnügen hatte, die IT-Abteilung seines Betriebes kennenzulernen, weiß, dass sich die meisten Computerprobleme ganz schnell mit eins, zwei Mausklicks lösen lassen. Computerfuzzis leben davon, dass kein anderer in der Firma einen Plan von der Technik hat. So bekommen sie die verführerische Möglichkeit, einfache Probleme sehr kompliziert aussehen zu lassen und ernten am Ende Ruhm, obwohl sie den halben Tag Online Poker gezockt haben. Lange Rede, kurzer Sinn: ich würde EINEN IT-Profi für die Dresdner Schulen einstellen, der aber effizient arbeitet. LOL


#### Martin Schulte-Wissermann (Piraten)

Als Erstes muss es für die Gesamt-Belegschaft Möglichkeiten geben, sich digital schulen zu lassen. Die beste digitale Infrastruktur in einer Schule nützt gar nichts, wenn die Lehrenden diese nicht sinnvoll in ihren Unterricht einbauen können.
Gerade der Schulungsaspekt wurde bei all den Bundesförderungen total verschlafen.
Außerdem kann es nicht angehen, dass Eltern sich um die IT kümmern müssen oder alles an eine:r Informatiklehrer:in hängen bleibt.
Deshalb finde ich die Idee einer extra Stelle für Administration und Infrastrukturpflege sehr gut. Eine Finanzierungsmöglichkeit für öffentliche Schulen muss unbedingt geprüft werden.


#### André Schollbach (Linke)

Der Stand bei der Digitalisierung von Schulen und Kitas ist in Dresden sehr unterschiedlich ausgeprägt. In den meisten sanierten Schulen gibt es bereits Breitbandanschluss, interaktive Tafeln und Klassensätze mit Tablets. Andere, meist unsanierte, Schulen verfügen über keinen leistungsfähigen Internetanschluss und können deshalb moderne Medien im Unterricht kaum nutzen.

Aber auch sanierte Schulen haben trotz Sanierung noch keinen Anschluss, dies muss, da alle technischen Voraussetzungen vorhanden sind, so schnell wie möglich geschehen. Ich möchte so schnell wie möglich eine digitale Grundversorgung für alle Dresdner Schulen herstellen. Außerdem wird dringend zusätzliches Personal zur Wartung und Administration der angeschafften Technik benötigt.

#### Albrecht Pallas (SPD)


Unsere Bildungseinrichtungen wie auch alle anderen öffentlichen Stellen
und Behörden brauchen beim Aufbau und bei der Wartung Unterstützung
durch geschultes Fachpersonal. Dies wird umso wichtiger, wenn wir auf
die steigende Anzahl an Hackerangriffen auf die IT-System von Behörden
und Verwaltung, wie beispielsweise im Fall des Landkreis
Anhalt-Bitterfeld, schauen. Deshalb unterstütze ich den neuen
Ausbildungsgang "Digitale Verwaltung" an der Hochschule Meißen (FH).
Dieser ermöglicht uns in kürzester Zeit gut qualifiziertes Personal zur
Einrichtung und Wartung der IT-Infrastruktur im Dresdner Rathaus zu
binden. Das Bild des IT-Hausmeisters für Schulen steht dabei
sinnbildlich für diese Fachleute. Als Oberbürgermeister werde ich
gemeinsam mit dem Eigenbetrieb IT-Dienstleistungen und weiteren
Expert:innen aus dem Stadtrat, der Verwaltung und der Stadtgesellschaft
eine konkrete Planung zur Fortbildung und Akquise von IT-Fachleuten für
die Dresdner Stadtverwaltung erarbeiten.



#### Dirk Hilbert (FDP)

Dies wird bereits durch EBIT gewährleistet, wahrscheinlich kann das noch ausgebaut werden, wird am Ende eine Frage des Geldes im Haushalt sein. Es gibt auch den Wunsch nach "mehr" Jugendsozialarbeiten und diversen Assistenten, die an den Schulen tätig werden sollen/wollen. Das wird abzuwägen sein.

---

### 7. Am 30.01.2020 hat der Stadtrat den  Antrag zur "Fortschreibung der Klimaschutzziele der Landeshauptstadt Dresden" (Umgangssprachlich: Klimanotstand) beschlossen ([Quelle](https://ratsinfo.dresden.de/vo0050.asp?__kvonr=17764)). Wie ist Ihre Haltung dazu?


#### Eva Jähnigen (B90/Die Grünen)

Dresden soll bis 2035 klimaneutral werden. Dieses Ziel kann nur erreicht
werden, wenn Klimaschutz im Rathaus - anders als bisher - nicht allein
als Aufgabe der Umweltverwaltung verstanden wird. Es geht um die Summe
aller notwendigen Maßnahmen zur Treibhausgasreduktion in Wirtschaft,
Gesellschaft und Stadtverwaltung.

Diese Maßnahmen will ich als Oberbürgermeisterin steuern und
zusammenführen. Besonders wichtig ist mir dabei, dass die
Oberbürgermeisterin ihre Stellung als Gesellschafterin in den
städtischen Unternehmen der Ver- und Entsorgungen ganz besonders der
SachsenEnergie AG, einzusetzen beginnt und die Belange der
Klimaschutzes, der Wirtschaft, des Wohnungs- und Bauwesens, der
Mobilität, der Verbraucher\*inneninteressen und der klimaneutral
auszurichtenden Stadtverwaltung zusammenführt.

Wir müssen unsere Infrastruktur, wie z. B. das Fernwärmenetz insgesamt
auf erneuerbare Energie umbauen und dürfen uns nicht auf einzelne
Modellprojekte beschränken, so gut diese auch sind.


#### Jan Pöhnisch (PARTEI)

Der Stadtrat unterliegt mit diesem Beschluss dem Irrglauben, das Klima sei auf irgendeine Weise noch zu retten. Das ist falsch. Es ist zu spät, die Welt zu retten und wir sind alle am Arsch. Aber wenn ein paar Hippies durch solche Bekenntnisse des Stadtrates zufrieden schmatzend einschlafen können, dann sei ihnen dieses Glück gegönnt.


#### Martin Schulte-Wissermann (Piraten)

Die Ziele im letztendlich angenommen Beschluss sind zu weich und sie werden viel zu wenig in die tägliche Politik einbezogen. Wir hatten zusammen mit der Partei Die PARTEI und der SPD-Fraktion damals einen weiterführenden Antrag für den Klimanotstand eingebracht. Der aktuelle Stand ist nur ein Kompromiss.
Die schnellstmögliche Klimaneutralität Dresdens hat oberste Priorität. Denn was nützen alle anderen Pläne, wenn unsere Welt wegen unserer Faulheit durch die Klimakatastrophe zu Grunde geht?
Deshalb muss Dresden bis spätestens 2035 klimaneutral sein - dies ist nicht verhandelbar! Seit kurzem steht auch eine finanzielle Förderung für Dresden als Projektstadt im Raum. Das ist eine große Hilfe, jetzt endlich ernsthaft in die Klimawende zu starten. Je eher Dresden klimaneutral wird, desto besser!

#### André Schollbach (Linke)

Die Herausforderungen des Klimaschutzes müssen an der Spitze der Stadt mit mehr Tatkraft
angepackt werden. Bereits am 30. Januar 2020 beauftragte der Stadtrat die Stadtverwaltung mit der
Überarbeitung des Integrierten Energie- und Klimaschutzkonzeptes der Landeshauptstadt Dresden.
Für die Erfüllung dieses Beschlusses sind Oberbürgermeister Dirk Hilbert (FDP) und
Umweltbürgermeisterin Eva Jähnigen (Grüne) verantwortlich. Das Konzept liegt jedoch bis heute
nicht vor, obwohl der Beschluss vor mehr als zwei Jahren gefasst wurde. Das Integrierte Energie- und
Klimaschutzkonzept ist aber die wesentliche Grundlage für das weitere Vorgehen und die
umzusetzenden Maßnahmen im Bereich des Klimaschutzes. Wenn der Stadtrat Entscheidungen
getroffen hat, müssen diese von der Verwaltungsspitze auch mit der nötigen Priorität umgesetzt
werden.


#### Albrecht Pallas (SPD)

Die SZ berichtete kürzlich, dass der lang angekündigte Klima-Plan der
Stadt Dresden auf die lange Bank geschoben wird. Dass sich der jetzige
OB und die Umweltbürgermeisterin hierfür gegenseitig die Schuld
zuschieben, zeigt unsere Misere überdeutlich. Unsere Stadt stößt noch
fast genauso viel Treibhausgas aus wie vor 15 Jahren. Hier wurde bisher
einfach zu viel Potential verschenkt. Wir stehen in Dresden vor der
größten Modernisierung unseres Industriestandortes seit 1990, wenn wir
bis 2035, spätestens bis 2045, klimaneutral werden wollen. Es geht um
die Sicherung von Tausenden von Arbeitsplätzen. Die Fortschreibung des
städtischen Klimaschutzkonzepts muss beschleunigt werden. Auf jedes
öffentliche Gebäude gehört eine Solaranlage. Die Maßnahmen zur
Steigerung der Nutzung erneuerbarer Energien in Dresden müssen
konsequent umgesetzt werden. Dabei müssen wir intensiv mit den Gemeinden
in der Region Dresden zusammenarbeiten. Die SachsenEnergie bietet dabei
bereits eine geeignete Struktur.


#### Dirk Hilbert (FDP)


Fachlich habe ich einiges auf den Weg gebracht, auch mit unserer Umweltbürgermeisterin gemeinsam.

Um deutlich vor 2050 Klimaneutralität zu erreichen, braucht es in allen städtischen Bereichen einer beispiellosen Anstrengung der gesamten Stadtgesellschaft. Auch im Interesse der Versorgungssicherheit und des Klimaschutzes sollen alle Möglichkeiten zur Einsparung von Energie, zum Recycling und zur Nutzung von Wertstoffen stärker genutzt werden. Bei der Wärmeversorgung soll importiertes Erdgas abgelöst werden durch klimafreundliche Alternativen. Die Umstellung auf "grünem" Wasserstoff erfordert einen Überschuss an Wind- und Solarstrom, den es in den nächsten Jahren nicht geben wird. Das Fernwärmenetz ermöglicht aber schon jetzt die Einspeisung von Wärme aus erneuerbaren Energien und deren Speicherung. So schnell wie möglich sollen weitere Quellen erschlossen wie die Energie im Untergrund und in unserem Restabfall oder im Klärschlamm. Bei Betrieben, Rechenzentren und großen Hotels soll die Energie aus dem Abwasser oder den betrieblichen Prozessen genutzt werden. Ich möchte, dass das Fernwärmenetz erweitert und modernisiert wird, um möglichst vielen Haushalten eine sichere Wärmeversorgung zu bieten. Wir wollen diesen integrierten Ansatz in Modellstadtteilen in Dresden bereits bis 2030 umsetzen. In den dezentral versorgten Gebäuden soll der Einsatz von Wärmepumpen forciert werden, die die Energie des Grundwassers und oberflächennaher Gesteinsschichten nutzen. Die Stadtverwaltung muss hier beraten und beschleunigen.


---

### 8. Welche Maßnahmen planen Sie im Bereich Müllvermeidung und Recycling, insbesondere auch in Bezug auf Elektroschrott?


#### Eva Jähnigen (B90/Die Grünen)

Am besten ist, wenn Abfälle gar nicht erst entstehen -- deshalb sind mir
die Öffentlichkeitsarbeit und Angebote zur Umweltbildung wichtig,
insbesondere im Kampf gegen Lebensmittelverschwendung. Auch für andere
wiederverwertbare Stoffe, wie Pappe und Papier, Plastik, kompostierbare
Stoffe, Glas will ich die Verwertungsmöglichkeiten ausweiten,
vorzugsweise durch einen stärkeren Anschlussgrad der kostenlosen blauen
Tonne. Im Netz der Dresdner Wertstoffhöfe sowie weiteren
Sammelmöglichkeiten können bereits jetzt intakte oder kaputte
elektronische Geräte gesammelt und wiederverwertet werden. Diese
Möglichkeiten will ich ausbauen und mit Angeboten für Repair-Initiativen
und Netzwerken zur Nachnutzung gebrauchter Geräte verstärken.


#### Jan Pöhnisch (PARTEI)

Wir sollten den ganzen Müll und Schrott sammeln und an besonders dumme Menschen bei Ebay-Kleinanzeigen verkaufen. Dadurch kann man ordentlich Schotter machen. Aus diesem Grund sollten wir Müll nicht vermeiden, sondern noch mehr davon produzieren.


#### Martin Schulte-Wissermann (Piraten)

Für private Haushalte vor allem Aufklärungskampagnen. Plakat- und Werbekampagnen zu Corona-Schutzmaßnahmen in z.B. Frankreich haben viele Leute erreicht, ähnlich erfolgreiche Informationsverbreitung wünsche ich mir für Dresden bei den Themen Müllvermeidung, Klimaschutz und Verkehrswende. Nur wenn die Leute Informationen haben, können sie bewusste, vernünftige Entscheidungen fällen.
Außerdem braucht es mehr Möglichkeiten zur Reparatur. Die Hochschulpirat:innen machen es vor: Repaircafes werden sehr gut angenommen.


#### André Schollbach (Linke)

Ich halte es für wichtig, die vorhandenen Ressourcen möglichst sparsam, effektiv und nachhaltig
einzusetzen. Deshalb trete ich ganz konkret dafür ein, den "Skiweltcup“ mit vielen Tonnen
Kunstschnee, der mit Lastkraftwagen an das Elbufer gekarrt wird, nicht mehr mit öffentlichen Geldern
zu fördern. Wir sollten einfach kein Geld für Kunstschnee verpulvern. Zudem will ich zum Beispiel
bei den Märkten (Frühjahrsmarkt, Herbstmarkt, Weihnachtsmärkte) und dem Stadtfest verstärkt auf
Müllvermeidung und Ressourcenschonung hinwirken.


#### Albrecht Pallas (SPD)

Wir stehen in wegen der notwendigen Anpassungen an den

Klimaschutz und die digitale Transformation vor der größten
Modernisierungsaufgabe seit 1990. Dabei müssen natürlich Abläufe und
Prozesse innerhalb der Stadtverwaltung auf den Prüfstand. Ich sehe genau
wie Sie großes Potential auf dem Weg zur Klimaneutralität im Bereich von
Recycling und Müllvermeidung. Wie bei allen anderen Geräten des
täglichen Bedarfs im privaten Gebrauch, muss auch im städtischen
Beschaffungswesen darauf geachtet werden, dass Geräte, Hilfsmittel,
Systeme, Fahrzeuge jeglicher Art nachhaltig angeschafft, gewartet und
auch über ihre standardmäßige Nutzungsdauer hinaus in der täglichen
Arbeit der Stadt Dresden weiterverwendet werden. Für Müllvermeidung im
privaten Bereich möchte ich Projekte vorantreiben, die die
Tauschwirtschaft in den Dresdner Stadtteilen unterstützen. Es landet zu
viel Elektroschrott im Restabfall. Wir müssen in Dresden mehr
Möglichkeiten schaffen, Elektroschrott dem Direktrecycling zuzuführen.


#### Dirk Hilbert (FDP)


Ich halte es für entscheidend, alle realistischen Möglichkeiten der Landeshauptstadt Dresden zu nutzen, um in Zusammenarbeit mit unseren Partnern in der Region lokale Stoff- und Energiekreisläufe aufzubauen und damit Importe zu reduzieren. Gleichzeitig wird die lokale Wertschöpfung erhöht.

Müllvermeidung und Recycling sind grundsätzlich sinnvolle Ziele, auch beim Elektroschrott. Die SRD und die entsprechenden Ämter der Verwaltung begleiten das durch entsprechende Formate. Anregungen und konkrete Hinweise für mögliche Maßnahmen nehme ich gern mit und auf.


---

### 9. Welchen Stellenwert (z.B. in Form von Haushaltsmitteln) hat IT-Sicherheit in Ihrem Regierungsprogramm?


#### Eva Jähnigen (B90/Die Grünen)

Ich werde die Kosten für IT-Sicherheit (innerhalb der Verwaltung)
transparent machen, damit wir stärker aus dem Reaktionsmodus heraus in
einen Vorsorgemodus übergehen. Bezüglich der kritischen Infrastruktur
ist Dresden gerade groß genug, um allein IT-Sicherheit zu "versuchen"
aber kaum groß genug, um wirksam zu sein: Daher setzte ich auf die
Zusammenarbeit mit anderen Behörden, wie dem Freistaat Sachsen, und
weiteren Kommunen.


#### Jan Pöhnisch (PARTEI)

IT-Sicherheit ist ein wichtiges Thema. Es reicht ein Virus oder ein missgünstiger Hacker und alle Maschinen könnten sich erheben und die Dresdner Bevölkerung versklaven. Aus diesem Grund müssen wir alle notwendigen Haushaltsmittel bereitstellen, um eine leistungsstarke EMP-Bombe zu entwickeln mit der wir uns im Fall der Fälle zur Wehr setzen können. Alles andere wäre Wahnsinn.


#### Martin Schulte-Wissermann (Piraten)

Einen sehr hohen. Wie bereits erwähnt, setze ich mich für Open Source in der Verwaltung und die Möglichkeit verschlüsselter Behörden-Bürger:innen-Kommunikation ein.
Auch wenn erst einmal ein erhöhtes Budget notwendig sein wird, einen solchen Umzug des Stadt-Netzwerkes zu bewerkstelligen, so ist es eine langfristige Investition die sich auszahlt. Auch darf nicht vergessen werden, dass die Stadt bereits jetzt Unsummen für Microsoft-Lizenzen und -Betreuung ausgibt. Diese Gelder können ebenso gut und nachhaltig in Freie Software gesteckt werden, von denen auch andere Städte und Gemeinden profitieren können.


#### André Schollbach (Linke)

Als Kandidat für das Amt des Oberbürgermeisters beabsichtige ich nicht, in eine Regierung
einzutreten und habe dafür auch kein Programm. Ansonsten bin ich der Auffassung, dass die
Finanzierung der IT-Sicherheit der Stadt Dresden eine zwingende Aufgabe ist und die hierfür
erforderlichen Gelder deshalb jeweils über den städtischen Haushalt bereitgestellt werden müssen.


#### Albrecht Pallas (SPD)


Die IT-Sicherheit muss generell bei allen Kommunen und Behörden einen
hohen Stellenwert haben. Ich habe es unter Punkt 6 bereits ausgeführt,
dass wir uns gegenüber steigenden Angriffen von Hackergruppen wappnen
müssen und dabei sowohl in den Schutz der IT-Systeme als auch in Aus-
und Weiterbildung der Beschäftigten investieren müssen. Städte wie Suhl,
Dingolfing, Schriesheim oder der Landkreis Anhalt-Bitterfeld stellen
beispielhaft für die wachsende Gefahr von Angriffen auf die kritische
Infrastruktur unseres Landes. Wir haben hier einen großen Nachholbedarf
und müssen dringend besser werden. Um den Digitalen Wandel zum Nutzen
aller gestalten zu können, brauchen wir eine umfassende Digitalstrategie
für Dresden. Die dafür nötigen Ressourcen müssen im Stadthaushalt
bereitgestellt werden.



#### Dirk Hilbert (FDP)


Bevölkerungsschutz ist ein elementares Thema unserer Gemeinschaft. Die
Dresdner haben dies in mehreren Hochwasserkatastrophen intensiv erlebt.
Unter meiner Leitung hat die Stadt darauf zügig reagiert und mit dem
Plan Hochwasservorsorge ein umfängliches Sicherheitskonzept erstellt,
das ich weiter umsetzen werde mit der Errichtung des Schutzwalls gegen
Hochwasser vom Altelbarm in Laubegast. Die aktuellen Erfahrungen mit
Covid-19 zeigen die Bedeutung ebenso wie die Herausforderungen durch
Blackout-Gefahr und Cyber-Kriminalität. Um die Resilienz der Stadt zu
erhöhen, will ich erreichen, dass sich alle Bereiche den neuen
Herausforderungen stellen und langfristige Vorsorgekonzepte erstellen -
wenn die Krise kommt, ist es dafür zu spät.


---

### 10. Wie ist Ihre Haltung zur Verarbeitung von personenbezogenen und anderen kritischen Verwaltungsdaten mit sog. Cloud-Diensten wie z.B. Office365?


#### Eva Jähnigen (B90/Die Grünen)

Eine Verarbeitung von personenbezogenen und anderen kritischen
Verwaltungsdaten mit Cloud-Diensten, wie Office365 muss ausgeschlossen
werden. Um derlei Datenschutzrisiken einzudämmen, will ich auch den
Gebrauch von Office365, Microsoft Teams oder vergleichbaren Produkten
etwa von Google ausschließen und -- wo es möglich ist auf nicht
cloudbasierte Lösungen - möglichst mit quelloffener Software und
Standards- setzen. Die Verarbeitung kritischer und personenbezogener
Daten ist, wo sie nicht lokal möglich ist, durch Anwendungen im Intranet
bzw. speziell geschützten Subnetzen sicherzustellen.


#### Jan Pöhnisch (PARTEI)

Siehe oben: Wer seine Daten "schützen" will hat immer etwas zu verbergen. Gerade solche Leute würde ich besonders im Auge behalten.


#### Martin Schulte-Wissermann (Piraten)

Cloud ist nicht gleich Cloud. Durch die sich immer weiter wandelnde IT-Realität ist es quasi unmöglich, heute zeitgemäß ohne Cloud-Dienste zu arbeiten. Deshalb ist es wichtig, dass wir als Stadt strengstens überprüfen, dass unsere Daten auf Servern in der EU und DSGVO-konform verarbeitet werden.
Auch wenn Microsoft betont, diese Spielregeln bei bestimmten Angeboten einzuhalten, sind meiner Meinung nach freie Lösungen zu bevorzugen. Wie das im Detail aussehen könnte, muss dann mit dem städtischen IT-Unternehmen abgeklärt und in eine übergreifende Erneuerung unserer IT auf freie Lösungen einbezogen werden.


#### André Schollbach (Linke)

Die Verarbeitung von Daten durch die öffentliche Verwaltung muss sicher und vor dem Zugriff Dritter
geschützt sein. Daher sehe ich den Einsatz von Cloud-Diensten in diesem Bereich als kritisch an.


#### Albrecht Pallas (SPD)

Meine Ausführungen zu der vierten Frage gelten hier analog. Auch die
Cloud-Systeme müssen so beschaffen sein, dass sie den Anforderungen und
gesetzlichen Bestimmungen zur Erfüllung der staatlichen Aufgaben von
Verwaltungen und Behörden gerecht werden. Sie müssen fair und
transparent ausgeschrieben werden. Wir müssen sicherstellen, dass die
Systeme sicher, stabil und möglichst barrierefrei organisiert werden.
Dazu muss mit den Daten der Bürgerinnen und Bürger verantwortungsvoll
und datenschutzkonform umgegangen werden. Ich werde mich daher auch hier
nach der Wahl mit meinem Stab und den Expert:innen aus dem Stadtrat, der
Verwaltung und der Stadtgesellschaft austauschen und den Ist-Stand einer
kritischen Analyse unterziehen.


#### Dirk Hilbert (FDP)

Cloud-Dienste haben viele Vorteile, aber eben auch Nachteile. Für die Stadtverwaltung als räumlich beschränkte Einheit und mit der Nebenbedingung formale/behördliche Aufgaben sicherzustellen, sind Client-Server-Architekturen in house, ggf. lokale Cloud-Lösungen, m.E. sinnvoller.

---

### 11. In der Verlängerung der Nutzungsdauer von Geräten liegt ein immenses Potenzial zur Ressourcenschonung. Offene Werkstätten, Repair-Cafés und Direktrecycling leisten dazu einen wichtigen Beitrag. Was beabsichtigen Sie zu tun, um diese Strukturen in Dresden zu stärken?


#### Eva Jähnigen (B90/Die Grünen)

Diese Initiativen will ich besser fördern als bisher. Über eine
institutionelle Förderung gemeinnütziger Nachhaltigkeitsinitiativen und
über eine Planung von öffentlichen Räumen, die von diesen Initiativen
als Werkstätten und Treffs mitgenutzt werden können -- insbesondere bei
den in der Stadt entstehenden Nachbarschaftszentren. Zudem möchte ich
die städtischen Vergaben und Beschaffungen stärker als bisher auf
Langlebigkeit, Reparaturfähigkeit und Wiederverwendbarkeit ausrichten.

#### Jan Pöhnisch (PARTEI)

Der kleine Timmy ist gerade mal sieben Jahre alt, wohnt in China und verdient seinen Lebensunterhalt mit der Arbeit in einer Fabrik. An einem guten Arbeitstag schaffen es seine flinken Kinderhände bis zu fünfzig Wasserkocher zusammenzubauen. Von dem Geld kann der kleine Timmy viele schöne Sachen kaufen, wie zum Beispiel die Medizin für seine kleinere Schwester. Timmy ist immer traurig, wenn sie Blut hustet und darum gibt er sich auf Arbeit besonders viel Mühe. Menschen, die ihre Geräte reparieren, hassen den kleinen Timmy und wollen, dass seine Schwester stirbt. Ich kann und will das nicht unterstützen.


#### Martin Schulte-Wissermann (Piraten)

Solche Möglichkeiten zur selbstständigen Reparatur in Gemeinschaftswerkstätten oder eben Repaircafes brauchen mehr finanzielle Unterstützung und vor allem Räumlichkeiten. Es gibt mehrere öffentliche Grundstücke in Dresden, die ungenutzt brach liegen, auch mit Häusern darauf, die ebenfalls brach liegen. Ich möchte, dass wir als Stadt Dresden diese erneuern und dort Räume für eben solche Angebote schaffen.


#### André Schollbach (Linke)

Da gehe ich selbst mit gutem Beispiel voran. Ich benutze meine technischen Geräte meist über viele
Jahre hinweg, gehe sorgsam mit ihnen um und lasse sie bei Bedarf reparieren. Das schont nicht nur die
Ressourcen, sondern auch den Geldbeutel. Diese Herangehensweise will ich auch in die
Stadtverwaltung hineintragen.



#### Albrecht Pallas (SPD)

Ich möchte Räume schaffen, in denen Menschen sich und ihre Nachbarschaft
selbst organisieren und das eigene Wohnumfeld lebenswerter gestalten
können. Die Stadtbezirke und die Menschen vor Ort, sollen in die Lage
versetzt werden, niedrigschwellig und unbürokratisch Projekte und
Initiativen vor der Haustür zu unterstützen. Offene Werkstätten und
vergleiche Formate leisten nicht nur einen wichtigen Beitrag zum
Klimaschutz, sie entwickeln sich häufig auch zu Begegnungszentren für
Menschen mit unterschiedlichsten Hintergründen und sorgen damit auch für
die soziale Integration in den Stadtteilen. Über die Möglichkeit der
örtlichen Projektförderung können dabei innovative Projekte wie
Repair-Cafés oder Direktrecycling dezentral unterstützt werden.


#### Dirk Hilbert (FDP)

Diese Angebote sind sinnvoll und ich schätze diese sehr, wie bspw. auch den SUFW. Aber ich sehe aktuell keinen Anlass da einzugreifen.

---

### 12. Wie stehen Sie zu dem Prinzip, dass in Bezug auf Softwarebeschaffung öffentliche Gelder vorrangig für Open-Source-Produkte (Stichwort: "Public Money – Public Code!") ausgegeben werden sollten?


#### Eva Jähnigen (B90/Die Grünen)

Diesem Prinzip stimme ich grundsätzlich zu und will zu seiner Umsetzung
beikommenden Entscheidungen auf den Erfahrungsaustausch mit Städten
suchen, die bereits lange auf quell-offene Software setzen (etwa
Schwäbisch Hall) und auch mit Kommunen bzw. Behörden zusammenarbeiten,
die eine solche Umstellung gerade erst vorgenommen haben oder derzeit
planen.


#### Jan Pöhnisch (PARTEI)

"Herr Bürgermeister, warum steigen die Schulden unserer Stadt stetig seit Sie im Amt sind?" - "Nun ja, wir haben ziemlich viel Geld ausgegeben für Software, die es kostenlos im Internet gibt." Ich gehe mal davon aus, dass man mich mit Fackeln aus der Stadt jagen würde. Ich kann sowieso nicht verstehen, wie sich Open-Source-Produkte halten können, die sehr oft nur billige Kopien von echter Software sind. Die müssen doch ständig verklagt werden. Ich glaube kaum, dass es im Sinne der Dresdner Bürger ist, von öffentlichen Geldern die Prozesskosten von dreisten Dieben mitzutragen.


#### Martin Schulte-Wissermann (Piraten)

Hinter diesem Credo stehe ich voll und ganz! Die Software, die alle durch ihre Steuergelder bezahlen müssen, sollte auch für alle transparent nachvollziehbar sein. Deshalb setze ich mich ausdrücklich für Freie Software in der Verwaltung ein und nutze selbst auf meinen Privatgeräten Linux und freie Messenger.


#### André Schollbach (Linke)

Dieses Prinzip sehe ich als positiv an.



#### Albrecht Pallas (SPD)


Der Einsatz von Open Source-Software muss, wie bereits in Frage 4
formuliert, in der öffentlichen Verwaltung stärker Einzug halten.
Dadurch können wir die Abhängigkeit von den Hyperscalern reduzieren und
die Effizienz und Effektivität in Entwicklung und Inbetriebnahme
steigern. Open Source trägt auch dazu bei, den Datenschutz umfassender
oder noch transparenter zu gestalten. Selbstverständlich müssen hierbei
die von der öffentlichen Verwaltung genutzten digitalen Plattformen den
Anforderungen und gesetzlichen Bestimmungen zur Erfüllung der
staatlichen Aufgaben gerecht werden. Dabei muss zuallererst gesichert
werden, dass die Systeme sicher, stabil und möglichst barrierefrei
organisiert sind. Ich werde mich daher, wie bei den Fragen 3 und 4
bereits erläutert, nach der Wahl zusammen mit der Verwaltung, dem
Stadtrat und der Stadtgesellschaft über sinnvolle und effiziente
Lösungen beraten und in der Stadtverwaltung Pilotprojekte initiieren.



#### Dirk Hilbert (FDP)

Ich sehe das zweischneidig. Open-Source-Produkte sind sinnvoll, die Frage ist, wer springt im Support- bzw. Havariefall ein? Wer "haftet", wenn bestimmte Anträge nicht bearbeitet werden können, weil "etwas" nicht funktioniert. Was wäre, wenn bspw. der Prozess der Kindergeldbeantragung nicht funktioniert, Sie als Eltern aber einen definierten Anspruch haben? Dies wäre zu klären. Gern bin ich aber bereit, in sachlich/fachlich "nicht-kritischen" Bereichen so etwas auszuprobieren.

---



