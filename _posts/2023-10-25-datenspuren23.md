---
layout: post
title: Datenspuren 2023
date: 2023-10-25 10:00
author: Nils 
summary: Infos zum Vortrag 'Alles EXCEL-lent an der TU-Dresden' von den Datenspuren 2023
image: /assets/images/2023-10-datenspuren23.jpg
---

# B&B Dresden auf den Datenspuren 2023

Wir waren auf den Datenspuren 2023 des C3D2 und haben dort einen Vortrag über Microsoft-Produkte an der TU-Dresden gehalten. 

## Alles EXCEL-lent an der TU-Dresden

Wir haben uns die Frage gestellt, wie viel Geld die TU-Dresden eigentlich für Microsoft Produkte bezahlt und einfach mal nachgefragt. Aber am Ende steckte hinter dieser kleinen Anfrage mehr Arbeit als erwartet. Wir nehmen euch mit auf dem Weg unserer Frage und wollen dabei ergründen, wer eigentlich mit Microsoft Verträge macht, warum alle Institutionen MS-Produkte nutzen, was eigentlich mit dem sächsischen Transparenz-Gesetz falsch ist, wie man sich vom MS-Monopol lösen kann und wie viel Geld die Unis nun wirklich ausgeben. 

**Veranstaltung:**

- [Vortragsvideo auf media.ccc.de](https://media.ccc.de/v/ds23-251-alles-excel-lent-an-der-tu-dresden)
- [Seite der Datenspuren 2023](https://datenspuren.de/2023/)

**Unsere Anfragen:**

- [IFG-Anfrage an Sächsisches Staatsministerium für Wissenschaft, Kultur und Tourismus (SMWK)](https://fragdenstaat.de/anfrage/rahmenvertrag-microsoft-campus-agreement-sachsen/)
- [IFG-Anfrage an TU Dresden](https://fragdenstaat.de/anfrage/microsoft-campus-agreement-an-der-tu-dresden/)
- [kleine Anfrage im Landtag](https://dresden.bits-und-baeume.org/assets/7_Drs_13259_1_1_1_.pdf)

**Weiterführende Quellen:**

*Vorträge und Dokus:*

- [Dokumentation "Das Microsoft Dilemma"](https://youtu.be/_7583HNrZJs)
- [Strategische Marktanalyse zur Reduzierung von Abhängigkeiten von einzelnen Software-Anbietern](https://web.archive.org/web/20220306092146/https://www.cio.bund.de/SharedDocs/Publikationen/DE/Aktuelles/20190919_strategische_marktanalyse.pdf?__blob=publicationFile)
- [Interview mit CIO von Schleswig-Holstein: Sven Thomson](https://youtu.be/MhVP1Ozc35s)
- [Vortrag "Linux Arbeitsplatz für die öffentliche Verwaltung" von den Chemnitzer Linux-Tagen](https://chemnitzer.linux-tage.de/2022/de/programm/beitrag/165)

*Datenschutz:*

- [Finale Feststellung der Datenschutzkonferenz zu Microsoft 365](https://datenschutzkonferenz-online.de/media/dskb/2022_24_11_festlegung_MS365.pdf)
- [Stellungnahme von Microsoft zum DSK-Bericht](https://news.microsoft.com/wp-content/uploads/prod/sites/40/2022/11/2022.11_Stellungnahme-MS-zu-DSK_25NOV2022_FINAL.pdf)

*Statistiken:*

- [Personal an sächsischen Hochschulen](https://www.statistik.sachsen.de/download/statistische-berichte/statistik-sachsen_bIII4_personal-hochschulen.xlsx)
- [Statistischer Jahresbericht der TU-Dresden 2021](https://tu-dresden.de/tu-dresden/profil/ressourcen/dateien/statjb/StatJB2021.pdf)
- [Jahresbericht der Uni Leipzig 2022](https://www.uni-leipzig.de/fileadmin/ul/Dokumente/2022_Jahresbericht.pdf)


