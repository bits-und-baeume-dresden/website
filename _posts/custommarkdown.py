"""
This script is a preprocessor to perform some custom markdown hacks.

Currently implemented:
- convert a unordered nested list into a flat list of <details>-tags


invocation:

py3 custommarkdown.py 2021-12-22-system-change-mit-instagram.md.raw

"""

import argparse

import markdown
from bs4 import BeautifulSoup

from ipydex import IPS, activate_ips_on_exception
activate_ips_on_exception()


parser = argparse.ArgumentParser()
parser.add_argument("source", help="the source file")

args = parser.parse_args()

assert args.source.endswith(".md.raw")

with open(args.source, "r") as txt_file:
    full_txt = txt_file.read()

lines = full_txt.split("\n")
target_path = args.source.replace(".md.raw", ".md")


final_lines = []
relevant_lines = []
relevance_flag = False
for l in lines:
    if l.startswith("<!-- <custommarkdown> -->"):
        relevance_flag = True
        final_lines.append("<replace-this>")
        continue
    if l.startswith("<!-- </custommarkdown> -->"):
        relevance_flag = False
        continue

    if relevance_flag:
        relevant_lines.append(l)
    else:
        final_lines.append(l)
    
relevant_block_txt = "\n".join(relevant_lines)

md = markdown.Markdown(extensions=[])
relevant_html = md.convert(relevant_block_txt)

soup = BeautifulSoup(relevant_html, 'html.parser')

ul_tags = soup.find_all("ul", recursive=False)
assert len(ul_tags) == 1

li_tags = ul_tags[0].find_all("li", recursive=False)


results = []

template = """
<!-- ------ -->
<details>
<summary>{}</summary>
{}
</details>
"""

for lt in li_tags:
    summary = str(lt.contents[0])
    body = "\n".join([str(ct) for ct in lt.contents[1:]])
    res = template.format(summary, body)
    results.append(res)


res_txt = "".join(results)
final_txt = "\n".join(final_lines).replace("<replace-this>", res_txt)

with open(target_path, "w") as txt_file:
    txt_file.write(final_txt)


print(f"{target_path} written")
