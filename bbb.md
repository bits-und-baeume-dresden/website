---
layout: post
title: Big Blue Button
summary: Big Blue Button Link
---

`BigBlueButton` ist ein Freies Quelloffenes Online-Konferenzsystem, speziell für den Bildungsbereich.

Bits und Bäume betreibt selber keine BBB-Instanz sondern nutzt nach Absprache die Instanzen anderer Organisationen.

Aktueller Hauptlink: <https://bbb.piratensommer.de/b/tro-7n3-6eh> (Instanz zur Verfügung gestellt von: Piraten Dresden)


---
<br>
<div style="font-size:85%;">
Ausweichlinks im Falle technischer Probleme:

<br>
<br>
 <a href="https://bbb.piratensommer.de/b/car-w6u-72d">https://bbb.piratensommer.de/b/car-w6u-72d</a> (Instanz: <a href="https://www.piraten-dresden.de/">Piraten Dresden</a>)


<br>
<br>
 <a href="https://blue.datenkollektiv.net/b/jan-nyz-ev9">https://blue.datenkollektiv.net/b/jan-nyz-ev9</a> (Instanz: <a href="https://datenkollektiv.net/">Datenkollektiv Dresden</a>)
</div>
